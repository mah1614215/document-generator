package com.tinubu.document.generator.openhtmltopdf.generator;

import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Pattern;

import com.openhtmltopdf.swing.NaiveUserAgent.DefaultUriResolver;

public class DocumentUriResolver extends DefaultUriResolver {
   private final static String DOCUMENT_SCHEME = "document://";

   @Override
   public String resolveURI(String baseUri, String uri) {
      if (isCssFile(baseUri)) {
         uri = String.format("%s%s%s", DOCUMENT_SCHEME, getParentUri(baseUri), uri);
      } else if (!uri.startsWith(DOCUMENT_SCHEME)) {
         uri = String.format("%s%s", DOCUMENT_SCHEME, uri);
      }

      return uri;
   }

   private boolean isCssFile(String baseUri) {
      return baseUri.toLowerCase().endsWith(".css");
   }

   private String getParentUri(String baseUri) {
      baseUri = baseUri.replaceAll(Pattern.quote("document://"), "");
      baseUri = Optional.ofNullable(Path.of(baseUri).getParent()).map(Path::toString).orElse("");

      if (!baseUri.isBlank()) {
         return String.format("%s/", baseUri);
      }

      return baseUri;
   }
}
