<#-- @ftlvariable name="zonedDateTime" type="java.time.ZonedDateTime" -->
<#-- @ftlvariable name="localDateTime" type="java.time.LocalDateTime" -->
<#-- @ftlvariable name="offsetDateTime" type="java.time.OffsetDateTime" -->
<#-- @ftlvariable name="timeFormat" type="java.lang.String" -->

<#-- Configured Locale-->
Locale : ${.locale_object}

<#-- ZONED DATE TIME-->
Full date : ${zonedDateTime}

Default format : ${zonedDateTime.format()}
Simple date : ${zonedDateTime.format('yyyy-MM-dd')}
Offset date time : ${zonedDateTime.format('ISO_OFFSET_DATE_TIME')}

Long date : ${zonedDateTime.format('LONG_DATE')}
Long time : ${zonedDateTime.format('LONG_TIME')}
Long date time : ${zonedDateTime.format('LONG_DATETIME')}

Medium date : ${zonedDateTime.format('MEDIUM_DATE')}
Medium time : ${zonedDateTime.format('MEDIUM_TIME')}
Medium date time : ${zonedDateTime.format('MEDIUM_DATETIME')}

Short date : ${zonedDateTime.format('SHORT_DATE')}
Short time : ${zonedDateTime.format('SHORT_TIME')}
Short date time : ${zonedDateTime.format('SHORT_DATETIME')}

Custom time format : ${zonedDateTime.format('HH:mm:ss')}
Custom full date : ${zonedDateTime.format('yyyy MM dd HH:mm: ss')}
Custom full date (seoul) : ${zonedDateTime.format('yyyy-MM-dd HH mm s Z', 'Asia/Seoul')}
Custom full date (paris) : ${zonedDateTime.format('yyyy-MM-dd HH mm s Z', 'Europe/Paris')}
Custom full date (UTC) : ${zonedDateTime.format('yyyy-MM-dd HH mm s Z', 'UTC')}
Custom full date (seoul yyyy-MM-dd) : ${zonedDateTime.format('yyyy-MM-dd', 'Asia/Seoul')}
Custom full date (paris yyyy-MM-dd) : ${zonedDateTime.format('yyyy-MM-dd', 'Europe/Paris')}
Custom full date (UTC yyyy-MM-dd) : ${zonedDateTime.format('yyyy-MM-dd', 'UTC')}

<#-- LOCAL DATE TIME-->
Default local time: ${localDateTime}
Default formatted local time: ${localDateTime.format()}
Local date time to zoned: ${localDateTime.toZonedDateTime()}

<#-- OFFSET DATE TIME-->
Default offset date time: ${offsetDateTime.format()}
Offset date time simple format: ${offsetDateTime.format('yyyy MM dd HH mm ss Z')}
Offset date time zoned format: ${offsetDateTime.format('yyyy MM dd HH mm ss Z', 'Asia/Seoul')}
Offset date time zoned format UTC: ${offsetDateTime.format('yyyy MM dd HH mm ss Z', 'UTC')}
Part time of offset date format: ${offsetDateTime.format(timeFormat)}
Part time of offset date 24h-format: ${offsetDateTime.format('HH:mm:ss')}