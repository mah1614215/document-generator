package com.tinubu.document.generator.freemarker;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_FTL;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.document.generator.core.support.DebugUtils.dumpDocument;
import static com.tinubu.document.generator.core.support.DebugUtils.openDocument;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.environment.ConfigurableEnvironment.ConfigurableEnvironmentBuilder;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.DocumentGenerator;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory;
import com.tinubu.document.generator.core.model.TextModel.TextModelBuilder;
import com.tinubu.document.generator.core.skin.CssSkin;
import com.tinubu.document.generator.core.skin.CssSkin.CssSkinBuilder;
import com.tinubu.document.generator.core.template.TextTemplate.TextTemplateBuilder;
import com.tinubu.document.generator.flyingsaucer.generator.FlyingSaucerDocumentGenerator.FlyingSaucerDocumentGeneratorBuilder;
import com.tinubu.document.generator.freemarker.generator.FreemarkerDocumentGenerator.FreemarkerDocumentGeneratorBuilder;
import com.tinubu.document.generator.freemarker.template.FtlTemplate.FtlTemplateBuilder;

class FreemarkerDocumentGeneratorTest {
   private static final ZonedDateTime FIXED_ZONED_DATE_TIME =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 21, 12, 14, 0), ZoneId.of("UTC"));
   private static final LocalDateTime FIXED_LOCAL_DATE_TIME = LocalDateTime.of(2021, 11, 21, 14, 35, 45);
   private static final OffsetDateTime FIXED_OFFSET_DATE_TIME =
         OffsetDateTime.of(2021, 11, 21, 15, 21, 30, 23, ZoneOffset.ofTotalSeconds(10800));

   @Nested
   public class TestSuiteWhenMemoryGeneratedRepository extends TestSuite {

      @BeforeEach
      public void configureGeneratedRepository() {
         GeneratedDocumentRepositoryFactory.memoryDocumentRepositoryFactory();
      }

   }

   @Nested
   public class TestSuiteWhenFsTempGeneratedRepository extends TestSuite {

      @BeforeEach
      public void configureGeneratedRepository() {
         GeneratedDocumentRepositoryFactory.fsTempDocumentRepositoryFactory();
      }

   }

   private abstract class TestSuite {

      @Test
      void generateDocument() throws DocumentGenerationException {
         DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();

         try (var html = generator
               .generateFromTemplate(new FtlTemplateBuilder()
                                           .templateRepository(templateRepository())
                                           .templateId(DocumentPath.of("template.ftlh"))
                                           .templateContentType(TEXT_HTML)
                                           .build(),
                                     new TextModelBuilder().data(dataModel()).build(),
                                     new CssSkinBuilder()
                                           .skinRepository(skinRepository())
                                           .skinId(DocumentPath.of("custom-styles.css"))
                                           .watermarkId(DocumentPath.of("sample-watermark.png"))
                                           .build())
               .loadContent()) {
            assertThat(html.document().content().stringContent())
                  .contains("color: blue")
                  .contains("<h1>Welcome Test User!</h1>")
                  .contains("<a href=\"products/greenmouse.html\">Green Mouse</a>!")
                  .contains("background-image: url('sample-watermark.png');");

            dumpDocument(html);

            try (var pdf = html.convert(new FlyingSaucerDocumentGeneratorBuilder().build())) {
               openDocument(pdf);
            }
         }
      }

      @Test
      void generateDocumentWhenNoopSkin() throws DocumentGenerationException {
         DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();

         try (var html = generator
               .generateFromTemplate(new FtlTemplateBuilder()
                                           .templateRepository(templateRepository())
                                           .templateId(DocumentPath.of("template.ftlh"))
                                           .templateContentType(TEXT_HTML)
                                           .build(),
                                     new TextModelBuilder().data(dataModel()).build(),
                                     CssSkin.noopSkin())
               .loadContent()) {
            assertThat(html.document().content().stringContent())
                  .contains("<link rel=\"stylesheet\" href=\"skin/styles.css\">")
                  .contains("<h1>Welcome Test User!</h1>")
                  .contains("<a href=\"products/greenmouse.html\">Green Mouse</a>!");

            openDocument(html);
         }
      }

      @Test
      void generateDocumentWithHeaderAndFooter() {
         DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();

         try (var html = generator
               .generateFromTemplate(new FtlTemplateBuilder()
                                           .templateId(DocumentPath.of("template.ftlh"))
                                           .templateContentType(TEXT_HTML)
                                           .headerId(DocumentPath.of("header.ftlh"))
                                           .footerId(DocumentPath.of("footer.ftlh"))
                                           .templateRepository(templateRepository())
                                           .build(),
                                     new TextModelBuilder().data(dataModel()).build(),
                                     CssSkin.noopSkin())
               .loadContent()) {

            assertThat(html).isNotNull();
            assertThat(html.document().content().stringContent())
                  .contains("class=\"header\"")
                  .contains("Pdf header secondary")
                  .contains("class=\"footer\"")
                  .contains("id=\"pdf-logo\"")
                  .contains("Pdf footer tinubu style")
                  .contains("class=\"pdf-page-number\"");

            openDocument(html);
         }

      }

      @Test
      void shouldNotContainsFooter_whenGenerateDocumentWithHeader() {
         DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();

         try (var html = generator
               .generateFromTemplate(new FtlTemplateBuilder()
                                           .templateId(DocumentPath.of("template.ftlh"))
                                           .templateContentType(TEXT_HTML)
                                           .headerId(DocumentPath.of("header.ftlh"))
                                           .templateRepository(templateRepository())
                                           .build(),
                                     new TextModelBuilder().data(dataModel()).build(),
                                     CssSkin.noopSkin())
               .loadContent()) {

            assertThat(html).isNotNull();
            assertThat(html.document().content().stringContent())
                  .contains("class=\"header\"")
                  .contains("Pdf header secondary")
                  .contains("id=\"pdf-logo\"")
                  .doesNotContain("class=\"footer\"")
                  .doesNotContain("Pdf footer tinubu style")
                  .doesNotContain("class=\"pdf-page-number\"");

            openDocument(html);
         }

      }

      @TestInstance(Lifecycle.PER_CLASS)
      @Nested
      class NestedFtlTemplateTest {
         @ParameterizedTest
         @MethodSource("templates")
         @DisplayName("Should return the mime type based on ftl template extension.")
         void autoDetectTemplateContentType(String template, MimeType mimeType) {
            var ftlTemplate = new FtlTemplateBuilder()
                  .templateId(DocumentPath.of(template))
                  .headerContent("HEADER")
                  .footerContent("FOOTER")
                  .templateRepository(templateRepository())
                  .build();

            assertThat(ftlTemplate.templateContentType()).isEqualTo(Optional.of(mimeType));
         }

         private Stream<Arguments> templates() {
            return Stream.of(arguments("template.ftlh", TEXT_HTML),
                             arguments("template.ftlx", APPLICATION_XML));
         }

         @Test
         @DisplayName("Should create ftl template from test template.")
         void ofTemplate() {
            var textTemplate = new TextTemplateBuilder()
                  .templateId(DocumentPath.of("template.ftlh"))
                  .templateRepository(templateRepository())
                  .build();

            var ftlTemplate =
                  FtlTemplateBuilder.ofTemplate(textTemplate).templateContentType(TEXT_FTL).build();

            assertThat(ftlTemplate.templateId()).isEqualTo(DocumentPath.of("template.ftlh"));
            assertThat(ftlTemplate.templateDocument()).isNotNull();
            assertThat(ftlTemplate.templateContentType()).isEqualTo(Optional.of(TEXT_FTL));
         }

         @Test
         @DisplayName("Should create ftl template from generated document.")
         void ofGeneratedDocument() {
            var document =
                  templateRepository().findDocumentById(DocumentPath.of("template.ftlh")).orElseThrow();

            try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build()) {
               var ftlTemplate = FtlTemplateBuilder.ofGeneratedDocument(generatedDocument).build();

               assertThat(ftlTemplate.templateId()).isEqualTo(DocumentPath.of("template.ftlh"));
               assertThat(ftlTemplate.templateContentType()).isEqualTo(Optional.of(TEXT_HTML));
               assertThat(ftlTemplate.templateDocument().content().stringContent(StandardCharsets.UTF_8))
                     .contains("background-image: url('${watermarkId.stringValue()}');")
                     .contains("<a href=\"${latestProduct.url}\">${latestProduct.name}</a>!");
            }
         }

         @Test
         @DisplayName("Should generate document with correct content.")
         void generateFromPlainText() {
            DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder().build();
            var document =
                  templateRepository().findDocumentById(DocumentPath.of("template.ftl")).orElseThrow();
            try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build();
                 var convertedDocument = generator.convert(generatedDocument)) {

               final DocumentContent documentContent = convertedDocument.document().content();

               assertThat(documentContent.contentEncoding()).isEqualTo(Optional.of(StandardCharsets.UTF_8));
               assertThat(documentContent.contentSize()).isEqualTo(Optional.of(257L));
               assertThat(documentContent.stringContent()).contains("<p> With plain text to test.</p>");
            }

         }
      }

      @Nested
      class NestedGeneratedDocumentWithExceptionTest {
         @Test
         @DisplayName("Should throw invariant validation exception.")
         void convertNullGeneratedDocument() {
            DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();
            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> generator.convert(null))
                  .withMessage("Invariant validation error > 'generatedDocument' must not be null");
         }

         @Test
         @DisplayName("Should throw document generation exception.")
         void convertGeneratedDocumentWithoutModel() {
            DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();
            var document =
                  templateRepository().findDocumentById(DocumentPath.of("template.ftlh")).orElseThrow();
            try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build()) {

               assertThatExceptionOfType(DocumentGenerationException.class)
                     .isThrownBy(() -> generator.convert(generatedDocument))
                     .withMessageContaining(
                           "- Failed at: ${user}  [in template \"template.ftlh\" at line 29, column 16]")
                     .withMessageContaining("==> user  [in template \"template.ftlh\" at line 29, column 18]");
            }
         }

         @Test
         @DisplayName("Should throw document generation exception.")
         void generateDocumentWithoutModel() {
            DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();

            var template = new FtlTemplateBuilder()
                  .templateId(DocumentPath.of("template.ftlh"))
                  .templateRepository(templateRepository())
                  .build();
            var textModel = new TextModelBuilder().build();
            var cssSkin = new CssSkinBuilder().skinRepository(skinRepository()).build();

            assertThatExceptionOfType(DocumentGenerationException.class)
                  .isThrownBy(() -> generator.generateFromTemplate(template, textModel, cssSkin))
                  .withMessageContaining(
                        "- Failed at: ${user}  [in template \"template.ftlh\" at line 29, column 16]")
                  .withMessageContaining("==> user  [in template \"template.ftlh\" at line 29, column 18]");
         }
      }

      @Test
      void dateAndTimeFormatTest() {
         DocumentGenerator generator = new FreemarkerDocumentGeneratorBuilder(environment()).build();

         var template = new FtlTemplateBuilder()
               .templateId(DocumentPath.of("dateTimeFormat.ftlh"))
               .templateRepository(templateRepository())
               .build();
         var textModel = new TextModelBuilder().data(new HashMap<>() {{
            put("zonedDateTime", FIXED_ZONED_DATE_TIME);
            put("localDateTime", FIXED_LOCAL_DATE_TIME);
            put("offsetDateTime", FIXED_OFFSET_DATE_TIME);
            put("timeFormat", "hh:mm:ss");
         }}).build();
         var cssSkin = new CssSkinBuilder().skinRepository(skinRepository()).build();

         try (var generatedDocument = generator.generateFromTemplate(template, textModel, cssSkin)) {
            assertThat(generatedDocument).isNotNull();
            assertThat(generatedDocument.document().content().stringContent())
                  .contains("Locale : en")
                  .contains("Full date : 2021-11-21T12:14Z[UTC]")
                  .contains("Offset date time : 2021-11-21T12:14:00Z")
                  .contains("Long date time : November 21, 2021 at 12:14:00 PM UTC")
                  .contains("Long time : 12:14:00 PM UTC")
                  .contains("Short time : 12:14")
                  .contains("Medium date time : Nov 21, 2021, 12:14:00 PM")
                  .contains("Custom time format : 12:14:00")
                  .contains("Custom full date : 2021 11 21 12:14: 00")
                  .contains("Custom full date (seoul) : 2021-11-21 21 14 0 +0900")
                  .contains("Custom full date (paris) : 2021-11-21 13 14 0 +0100")
                  .contains("Custom full date (UTC) : 2021-11-21 12 14 0 +0000")
                  .contains("Custom full date (seoul yyyy-MM-dd) : 2021-11-21")
                  .contains("Custom full date (paris yyyy-MM-dd) : 2021-11-21")
                  .contains("Custom full date (UTC yyyy-MM-dd) : 2021-11-21")
                  .contains("Default local time: 2021-11-21T14:35:45")
                  .contains("Default formatted local time: 2021-11-21T14:35:45")
                  .contains("Default offset date time: 2021-11-21T15:21:30.000000023+03:00")
                  .contains("Offset date time simple format: 2021 11 21 15 21 30")
                  .contains("Offset date time zoned format: 2021 11 21 21 21 30 +0900")
                  .contains("Offset date time zoned format UTC: 2021 11 21 12 21 30 +0000")
                  .contains("Part time of offset date format: 03:21:30")
                  .contains("Part time of offset date 24h-format: 15:21:30");
         }
      }
   }

   private static Environment environment() {
      return ConfigurableEnvironmentBuilder
            .ofEnvironment(SystemEnvironment.ofSystemDefaults())
            .timeZone(ZoneId.of("Europe/Paris"))
            .locale(Locale.ENGLISH)
            .documentEncoding(StandardCharsets.UTF_8)
            .build();
   }

   private static ClasspathDocumentRepository templateRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/freemarker"));
   }

   private static DocumentRepository skinRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/freemarker/skin"));
   }

   private Map<String, Object> dataModel() {
      Map<String, Object> model = new HashMap<>();
      model.put("user", "Test User");
      model.put("latestProduct", new Product("products/greenmouse.html", "Green Mouse"));
      return model;
   }

   /**
    * Product sample bean.
    */
   public static class Product {

      private final String url;
      private final String name;

      public Product(String url, String name) {
         this.url = url;
         this.name = name;
      }

      public String getUrl() {
         return url;
      }

      public String getName() {
         return name;
      }

   }
}