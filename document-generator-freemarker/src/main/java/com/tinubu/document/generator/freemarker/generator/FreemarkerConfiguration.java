package com.tinubu.document.generator.freemarker.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;

import java.util.List;
import java.util.TimeZone;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.document.generator.core.environment.Environment;

import freemarker.core.HTMLOutputFormat;
import freemarker.core.OutputFormat;
import freemarker.core.UndefinedOutputFormat;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import no.api.freemarker.java8.Java8ObjectWrapper;

public class FreemarkerConfiguration {

   private static final List<OutputFormat> STANDARD_OUTPUT_FORMATS = list(HTMLOutputFormat.INSTANCE);

   private final Configuration configuration;

   public FreemarkerConfiguration(Environment environment) {
      this.configuration = initializeConfiguration(environment);
   }

   public FreemarkerConfiguration(FreemarkerConfiguration freemarkerConfiguration) {
      this.configuration = (Configuration) freemarkerConfiguration.configuration.clone();
   }

   public Configuration configuration() {
      return configuration;
   }

   public FreemarkerConfiguration mergeSettings(FreemarkerConfigurer config) {
      validate(config, "config", isNotNull()).orThrow();

      config.accept(configuration);
      return this;
   }

   public <T> FreemarkerConfiguration setSettingIfAbsent(Function<Configuration, T> getter,
                                                         BiConsumer<Configuration, T> setter,
                                                         T value) {
      validate(getter, "getter", isNotNull()).orThrow();
      validate(setter, "setter", isNotNull()).orThrow();

      return mergeSettings(config -> {
         if (getter.apply(config) == null) {
            setter.accept(config, value);
         }
      });
   }

   public <T> FreemarkerConfiguration setSetting(BiConsumer<Configuration, T> setter, T value) {
      validate(setter, "setter", isNotNull()).orThrow();

      return mergeSettings(config -> setter.accept(config, value));
   }

   public <T> FreemarkerConfiguration setSetting(String name, String value) {
      validate(name, "name", isNotBlank()).orThrow();

      return mergeSettings(config -> {
         try {
            config.setSetting(name, value);
         } catch (TemplateException e) {
            throw new IllegalStateException(e);
         }
      });
   }

   public List<OutputFormat> standardOutputFormats() {
      return STANDARD_OUTPUT_FORMATS;
   }

   public List<OutputFormat> outputFormats() {
      return list(streamConcat(stream(standardOutputFormats()),
                               stream(this.configuration.getRegisteredCustomOutputFormats())));
   }

   public FreemarkerConfigurer configureOutputFormat(MimeType contentType, List<OutputFormat> outputFormats) {
      return config -> nullable(contentType).ifPresent(ct -> {
         OutputFormat currentOutputFormat = config.getOutputFormat();
         if (currentOutputFormat.getName().equals(UndefinedOutputFormat.INSTANCE.getName())) {
            stream(outputFormats)
                  .filter(outputFormat -> ct.equalsTypeAndSubtype(parseMimeType(outputFormat.getMimeType())))
                  .findFirst()
                  .ifPresent(config::setOutputFormat);
         }
      });
   }

   public FreemarkerConfigurer configureOutputFormat(MimeType contentType) {
      return configureOutputFormat(contentType, outputFormats());
   }

   /**
    * Creates configuration instance that is the central place to store the application level settings
    * of Freemarker.
    *
    * @return configuration instance
    */
   protected static Configuration initializeConfiguration(Environment environment) {
      Configuration config = new Configuration(Configuration.VERSION_2_3_31);

      config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
      config.setLogTemplateExceptions(false);
      config.setWrapUncheckedExceptions(true);
      config.setFallbackOnNullLoopVariable(false);
      config.setLocalizedLookup(true);
      config.setRecognizeStandardFileExtensions(true);

      // See https://github.com/lazee/freemarker-java-8 for details.
      config.setObjectWrapper(new Java8ObjectWrapper(Configuration.VERSION_2_3_31));

      if (environment != null) {
         config.setDefaultEncoding(environment.documentEncoding().name());
         config.setTimeZone(TimeZone.getTimeZone(environment.timeZone()));
         config.setLocale(environment.locale());
         config.setDateFormat(environment.dateFormat());
         config.setTimeFormat(environment.timeFormat());
         config.setDateTimeFormat(environment.dateTimeFormat());
      }

      return config;
   }

   public interface FreemarkerConfigurer extends Consumer<Configuration> { }

}