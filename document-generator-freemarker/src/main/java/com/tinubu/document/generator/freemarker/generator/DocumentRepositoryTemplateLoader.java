package com.tinubu.document.generator.freemarker.generator;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.Reader;
import java.nio.charset.Charset;
import java.time.Instant;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

import freemarker.cache.TemplateLoader;

/**
 * Freemarker template loader for a {@link DocumentRepository}.
 */
public class DocumentRepositoryTemplateLoader implements TemplateLoader {

   private final DocumentRepository documentRepository;
   private final DocumentProcessor documentProcessor;

   public DocumentRepositoryTemplateLoader(DocumentRepository documentRepository,
                                           DocumentProcessor documentProcessor) {
      this.documentRepository = notNull(documentRepository, "documentRepository");
      this.documentProcessor = documentProcessor;
   }

   public DocumentRepositoryTemplateLoader(DocumentRepository documentRepository) {
      this(documentRepository, null);
   }

   @Override
   public Object findTemplateSource(String name) {
      return documentRepository
            .findDocumentById(DocumentPath.of(name))
            .map(document -> nullable(documentProcessor).map(document::process).orElse(document))
            .orElse(null);
   }

   @Override
   public long getLastModified(Object templateSource) {
      return ((Document) templateSource).metadata().lastUpdateDate().map(Instant::toEpochMilli).orElse(-1L);
   }

   @Override
   public Reader getReader(Object templateSource, String encoding) {
      return (((Document) templateSource).content().readerContent(Charset.forName(encoding)));
   }

   @Override
   public void closeTemplateSource(Object templateSource) {
   }
}
