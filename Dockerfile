#### Build environment image ####

FROM maven:3.8.4-openjdk-17-slim AS build

ENV FONTCONFIG_PACKAGE=fontconfig FONTCONFIG_VERSION=2.13.1-4.2

# Install packages.

# fontconfig is required by Asciidoctor generator
RUN apt-get -y update && apt-get -y install ${FONTCONFIG_PACKAGE}=${FONTCONFIG_VERSION} && apt-get -y clean

ENV MAVEN_OPTS=""
ENV MAVEN_CLI_OPTS="-B --show-version -Dstyle.color=always"
ENV BUILD_DIR="/build"

WORKDIR "$BUILD_DIR"

# Caching dependencies
COPY .mvn .mvn
COPY pom.xml .
COPY document-generator-core/pom.xml document-generator-core/
COPY document-generator-freemarker/pom.xml document-generator-freemarker/
COPY document-generator-flying-saucer/pom.xml document-generator-flying-saucer/
COPY document-generator-asciidoctor/pom.xml document-generator-asciidoctor/
COPY document-generator-docx4j/pom.xml document-generator-docx4j/
COPY document-generator-apache-poi/pom.xml document-generator-apache-poi/
COPY document-generator-openhtmltopdf/pom.xml document-generator-openhtmltopdf/

RUN mvn $MAVEN_CLI_OPTS dependency:go-offline -Dsilent=true

COPY . .

ARG SITE=false
ARG SITE_EXPORT=/export/site.tar.gz
ARG SKIP_TESTS=false
ARG TESTS_EXPORT=/export/tests.tar.gz
ARG CI_JOB_TOKEN
ENV CI_JOB_TOKEN="$CI_JOB_TOKEN"
ARG CI_PROJECT_ID
ENV CI_PROJECT_ID="$CI_PROJECT_ID"

RUN set -e; \
    cp .docker/buildsdk /bin; \
    eval buildsdk setx mvn $MAVEN_CLI_OPTS -s .mvn/maven-settings.xml clean deploy $($SKIP_TESTS && echo "-DskipTests") -Dmaven.test.failure.ignore -DenableJacoco; \
    if ! $SKIP_TESTS; then \
    buildsdk check_tests; \
    buildsdk report_jacoco_coverage; \
    mkdir -p "$(dirname "$TESTS_EXPORT")" && tar czf "$TESTS_EXPORT" $(find . -path '*/target/*-reports/TEST-*.xml'); \
    fi; \
    if $SITE; then \
       buildsdk setx mvn $MAVEN_CLI_OPTS site site:stage -DenableJavadocReporting -DskipTests && (cd target/staging && mkdir -p "$(dirname "$SITE_EXPORT")" && tar czf "$SITE_EXPORT" .); \
    fi
