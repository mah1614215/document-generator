package com.tinubu.document.generator.apachepoi.spreadsheet.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;

import com.tinubu.document.generator.apachepoi.spreadsheet.generator.SpreadSheetDocumentGenerator.ModelSheetVisibility;
import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.model.SpreadsheetModel.Table;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;

public class ModelSheetsProcessor extends AbstractWorkbookProcessor {

   private final ModelSheetVisibility modelSheetVisibility;

   public ModelSheetsProcessor(ModelSheetVisibility modelSheetVisibility) {
      this.modelSheetVisibility = notNull(modelSheetVisibility, "modelSheetVisibility");
   }

   @Override
   public Workbook process(Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model) {
      notNull(workbook, "workbook");
      notNull(template, "template");
      notNull(model, "model");

      if (modelSheetVisibility != ModelSheetVisibility.NONE) {
         model.tables().forEach((name, table) -> {
            String tableSheetName = normalizeSheetName(name);
            Sheet tableSheet = workbook.createSheet(tableSheetName);
            hideSheet(workbook, fillSheet(tableSheet, table), modelSheetVisibility.sheetState());
            tableName(workbook, tableSheetName, table);
         });
      }

      return workbook;
   }

   private Name tableName(Workbook workbook, String tableSheetName, Table table) {
      Name tableName = workbook.createName();
      tableName.setNameName(tableSheetName);
      tableName.setComment(String.format("'%s' model table", tableSheetName));
      tableName.setRefersToFormula(tableFormula(tableSheetName, table));
      tableName.setSheetIndex(-1);
      return tableName;
   }

   private String tableFormula(String tableSheetName, Table table) {
      return new AreaReference(new CellReference(tableSheetName, 0, 0, true, true),
                               new CellReference(tableSheetName,
                                                 table.rows() - 1,
                                                 table.columns() - 1,
                                                 true,
                                                 true)).formatAsString();
   }

   private Workbook hideSheet(Workbook workbook, Sheet sheet, int hidden) {
      int sheetIndex = workbook.getSheetIndex(sheet);
      workbook.setSheetHidden(sheetIndex, hidden);
      return workbook;
   }

}
