package com.tinubu.document.generator.apachepoi.spreadsheet.processor;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.tinubu.document.generator.apachepoi.spreadsheet.generator.exception.TooManyColumnsException;
import com.tinubu.document.generator.apachepoi.spreadsheet.generator.exception.TooManyRowsException;
import com.tinubu.document.generator.core.model.SpreadsheetModel.Table;

public abstract class AbstractWorkbookProcessor implements WorkbookProcessor {

   protected Sheet sheet(Workbook workbook, int sheetIndex) {
      return workbook.getSheetAt(sheetIndex);
   }

   protected Sheet sheet(Workbook workbook, String sheetName) {
      return workbook.getSheet(sheetName);
   }

   protected SpreadsheetVersion spreadsheetVersion(Workbook workbook) {
      return spreadsheetVersion(sheet(workbook, 0));
   }

   protected SpreadsheetVersion spreadsheetVersion(Sheet sheet) {
      if (sheet instanceof XSSFSheet) {
         return SpreadsheetVersion.EXCEL2007;
      } else if (sheet instanceof HSSFSheet) {
         return SpreadsheetVersion.EXCEL97;
      } else {
         throw new IllegalStateException("Unknown version");
      }
   }

   protected String normalizeSheetName(String name) {
      return WorkbookUtil.createSafeSheetName(name);
   }

   /**
    * Sets a cell with model value.
    *
    * @param cell cell to set
    * @param value value to set in cell
    */
   protected void setCellValue(Cell cell, Object value) {
      if (value instanceof Date) {
         cell.setCellValue((Date) value);
      } else if (value instanceof Calendar) {
         cell.setCellValue((Calendar) value);
      } else if (value instanceof RichTextString) {
         cell.setCellValue((RichTextString) value);
      } else if (value instanceof Boolean) {
         cell.setCellValue((Boolean) value);
      } else if (value instanceof Number) {
         cell.setCellValue(((Number) value).doubleValue());
      } else {
         cell.setCellValue(nullable(value).map(String::valueOf).orElse(null));
      }
   }

   protected Sheet fillSheet(Sheet sheet, Table table, int sheetRow, int sheetColumn) {
      SpreadsheetVersion spreadsheetVersion = spreadsheetVersion(sheet);
      checkTable(table,
                 spreadsheetVersion.getMaxRows() - sheetRow,
                 spreadsheetVersion.getMaxColumns() - sheetColumn);

      for (int i = 0; i < table.rows(); i++) {
         Row row = sheet.createRow(sheetRow + i);

         for (int j = 0; j < table.columns(); j++) {
            Cell cell = row.createCell(sheetColumn + j);
            setCellValue(cell, table.data(i, j));
         }
      }
      return sheet;
   }

   protected Sheet fillSheet(Sheet sheet, Table table) {
      return fillSheet(sheet, table, 0, 0);
   }

   /**
    * Throws exception when the model table exceeds worksheet rows and/or columns limits.
    *
    * @param table table to check
    * @param maxRows max rows supported
    * @param maxColumns max columns supported
    *
    * @throws TooManyRowsException if table rows exceeds worksheet rows limit
    * @throws TooManyColumnsException if table columns exceeds worksheet columns limit
    */
   protected void checkTable(Table table, int maxRows, int maxColumns) {
      int numberOfRows = table.rows();
      if (numberOfRows > maxRows) {
         throw new TooManyRowsException(numberOfRows, maxRows);
      }

      int numberOfColumns = table.columns();
      if (numberOfColumns > maxColumns) {
         throw new TooManyColumnsException(numberOfColumns, maxColumns);
      }
   }

}
