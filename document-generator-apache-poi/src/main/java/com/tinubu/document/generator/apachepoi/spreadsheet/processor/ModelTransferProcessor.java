package com.tinubu.document.generator.apachepoi.spreadsheet.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.model.SpreadsheetModel.Table;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;

public abstract class ModelTransferProcessor extends AbstractWorkbookProcessor {

   @Override
   public Workbook process(Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model) {
      notNull(workbook, "workbook");
      notNull(template, "template");
      notNull(model, "model");

      transferModel(workbook, model);

      return workbook;
   }

   public abstract void transferModel(Workbook workbook, SpreadsheetModel model);

   protected Sheet transferModel(Sheet sheet, Table table, int sheetRow, int sheetColumn) {
      return fillSheet(sheet, table, sheetRow, sheetColumn);
   }

   protected Sheet transferModel(Sheet sheet, Table table) {
      return fillSheet(sheet, table);
   }
}
