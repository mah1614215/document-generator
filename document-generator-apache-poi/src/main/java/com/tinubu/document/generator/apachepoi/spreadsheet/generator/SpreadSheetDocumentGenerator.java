package com.tinubu.document.generator.apachepoi.spreadsheet.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.document.generator.apachepoi.spreadsheet.processor.WorkbookProcessor.identity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.functions.FreeRefFunction;
import org.apache.poi.ss.formula.udf.AggregatingUDFFinder;
import org.apache.poi.ss.formula.udf.DefaultUDFFinder;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.document.generator.apachepoi.spreadsheet.processor.EvaluateFormulasProcessor;
import com.tinubu.document.generator.apachepoi.spreadsheet.processor.ModelSheetsProcessor;
import com.tinubu.document.generator.apachepoi.spreadsheet.processor.ReplacePlaceholdersProcessor;
import com.tinubu.document.generator.apachepoi.spreadsheet.processor.WorkbookProcessor;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.AbstractDocumentGenerator;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.model.Model;
import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.skin.NoopSkin;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate.SpreadsheetTemplateBuilder;
import com.tinubu.document.generator.core.template.Template;

public class SpreadSheetDocumentGenerator extends AbstractDocumentGenerator {

   private static final MimeType XLSX_CONTENT_TYPE =
         parseMimeType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

   /** Whether to create a new sheet per model table in the resulting spreadsheet. */
   private final ModelSheetVisibility modelSheetVisibility;
   private final WorkbookProcessor preProcessor;
   private final WorkbookProcessor postProcessor;

   protected SpreadSheetDocumentGenerator(SpreadSheetDocumentGeneratorBuilder builder) {
      super(builder.environment);
      this.modelSheetVisibility = nullable(builder.modelSheetsVisibility, ModelSheetVisibility.HIDDEN);
      this.preProcessor = nullable(builder.preProcessor, identity());
      this.postProcessor = nullable(builder.postProcessor, identity());
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends SpreadSheetDocumentGenerator> defineDomainFields() {
      return Fields
            .<SpreadSheetDocumentGenerator>builder()
            .superFields((Fields<SpreadSheetDocumentGenerator>) super.defineDomainFields())
            .build();
   }

   @Override
   public GeneratedDocument generateFromTemplate(Template template, Model model, Skin skin) {
      validate(template, "template", isInstanceOf(value(SpreadsheetTemplate.class))).orThrow();
      validate(model, "model", isInstanceOf(value(SpreadsheetModel.class))).orThrow();
      validate(skin, "skin", isInstanceOf(value(NoopSkin.class))).orThrow();

      try {
         return generateDocument((SpreadsheetTemplate) template, (SpreadsheetModel) model);
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   @Override
   public GeneratedDocument convert(GeneratedDocument generatedDocument) {
      validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

      try {
         return generateDocument(SpreadsheetTemplateBuilder.ofGeneratedDocument(generatedDocument).build(),
                                 SpreadsheetModel.noopModel());
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   private GeneratedDocument generateDocument(SpreadsheetTemplate template, SpreadsheetModel model) {

      Workbook workbook = preProcessor
            .andThen((wb, __, m) -> {
               wb.setForceFormulaRecalculation(true);
               registerFunction(wb, new ModelFunction(m));
               return wb;
            })
            .andThen(new ModelSheetsProcessor(modelSheetVisibility))
            .andThen(new ReplacePlaceholdersProcessor())
            .andThen(new EvaluateFormulasProcessor())
            .andThen(postProcessor)
            .process(workbook(template), template, model);

      try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
         workbook.write(outputStream);

         DocumentContent content = new InputStreamDocumentContentBuilder()
               .content(outputStream.toByteArray(), StandardCharsets.UTF_8)
               .build();

         return new GeneratedDocumentBuilder()
               .document(outputDocument(template.templateId(), content, XLSX_CONTENT_TYPE))
               .documentRepository(template.templateRepository())
               .build();

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private void registerFunction(Workbook workbook, ModelFunction function) {
      UDFFinder udfs =
            new DefaultUDFFinder(new String[] { function.name() }, new FreeRefFunction[] { function });
      UDFFinder udfToolPack = new AggregatingUDFFinder(udfs);

      workbook.addToolPack(udfToolPack);
   }

   private Workbook workbook(SpreadsheetTemplate template) {
      final Document templateDocument = template.templateDocument();

      try (InputStream templateInputStream = templateDocument.content().inputStreamContent()) {
         return WorkbookFactory.create(templateInputStream);
      } catch (IOException | InvalidFormatException e) {
         throw new IllegalStateException(e);
      }
   }

   public enum ModelSheetVisibility {
      VISIBLE(Workbook.SHEET_STATE_VISIBLE),
      HIDDEN(Workbook.SHEET_STATE_HIDDEN),
      VERY_HIDDEN(Workbook.SHEET_STATE_VERY_HIDDEN),
      NONE(-1);

      private final int sheetState;

      ModelSheetVisibility(int sheetState) {
         this.sheetState = sheetState;
      }

      public int sheetState() {
         return sheetState;
      }
   }

   public static class SpreadSheetDocumentGeneratorBuilder
         extends DomainBuilder<SpreadSheetDocumentGenerator> {
      private final Environment environment;
      private ModelSheetVisibility modelSheetsVisibility;
      private WorkbookProcessor preProcessor;
      private WorkbookProcessor postProcessor;

      public SpreadSheetDocumentGeneratorBuilder(Environment environment) {
         this.environment = validate(environment, "environment", isNotNull()).orThrow();
      }

      public SpreadSheetDocumentGeneratorBuilder() {
         this(SystemEnvironment.ofSystemDefaults());
      }

      public SpreadSheetDocumentGeneratorBuilder modelSheetsVisibility(ModelSheetVisibility modelSheetsVisibility) {
         this.modelSheetsVisibility = modelSheetsVisibility;
         return this;
      }

      public SpreadSheetDocumentGeneratorBuilder preProcessor(WorkbookProcessor preProcessor) {
         this.preProcessor = preProcessor;
         return this;
      }

      public SpreadSheetDocumentGeneratorBuilder postProcessor(WorkbookProcessor postProcessor) {
         this.postProcessor = postProcessor;
         return this;
      }

      @Override
      public SpreadSheetDocumentGenerator buildDomainObject() {
         return new SpreadSheetDocumentGenerator(this);
      }
   }
}
