package com.tinubu.document.generator.apachepoi.spreadsheet.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.apache.poi.ss.usermodel.Workbook;

import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;

public class EvaluateFormulasProcessor extends AbstractWorkbookProcessor {

   @Override
   public Workbook process(Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model) {
      notNull(workbook, "workbook");
      notNull(template, "template");
      notNull(model, "model");

      workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
      return workbook;
   }

}
