package com.tinubu.document.generator.apachepoi.spreadsheet.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.apache.poi.ss.usermodel.Workbook;

import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;

/**
 * General {@link Workbook} processor.
 */
@FunctionalInterface
public interface WorkbookProcessor {

   /**
    * Processes specified workbook.
    *
    * @param workbook workbook to process
    *
    * @return new processed workbook instance
    */
   Workbook process(Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model);

   default WorkbookProcessor compose(WorkbookProcessor before) {
      notNull(before, "before");
      return (Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model) -> process(before.process(
            workbook,
            template,
            model), template, model);
   }

   default WorkbookProcessor andThen(WorkbookProcessor after) {
      notNull(after, "after");
      return (Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model) -> after.process(
            process(workbook, template, model),
            template,
            model);
   }

   static WorkbookProcessor identity() {
      return (workbook, template, model) -> workbook;
   }

}