package com.tinubu.document.generator.apachepoi.spreadsheet.generator.exception;

import java.text.MessageFormat;

/**
 * Indicates that the total number of rows for an object is exceeded.
 */
public class TooManyRowsException extends RuntimeException {
   private static final MessageFormat DEFAULT_MESSAGE =
         new MessageFormat("You provide {0} rows which exceeds {1} equal to maximum limit of rows");

   public TooManyRowsException(String message) {
      super(message);
   }

   public TooManyRowsException(int numberOfColumn, int maxLimit) {
      this(DEFAULT_MESSAGE.format(new Object[] { numberOfColumn, maxLimit }));
   }
}
