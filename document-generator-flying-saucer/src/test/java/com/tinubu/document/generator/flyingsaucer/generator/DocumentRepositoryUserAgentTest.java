package com.tinubu.document.generator.flyingsaucer.generator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;

class DocumentRepositoryUserAgentTest {

   DocumentRepositoryUserAgent userAgent;

   @BeforeEach
   void init() {
      userAgent = new DocumentRepositoryUserAgent(null, new MemoryDocumentRepository());
   }

   @Test
   void testUserAgentWhenNominal() {
      userAgent.setBaseURL(".");
      assertThat(userAgent.resolveURI("file.png")).isEqualTo("file.png");
   }

   @Test
   void testUserAgentWhenSchemedBaseUrl() {
      userAgent.setBaseURL("file:/path/");
      assertThat(userAgent.resolveURI("file.png")).isEqualTo("file:/path/file.png");
   }

   @Test
   void testUserAgentWhenSchemedResolveUri() {
      userAgent.setBaseURL(".");
      assertThat(userAgent.resolveURI("file:/file.png")).isEqualTo("file:/file.png");
   }

   @Test
   void testUserAgentWhenNullBaseUrl() {
      userAgent.setBaseURL(null);
      assertThat(userAgent.resolveURI("file.png")).isEqualTo("file.png");
   }

   @Test
   void testUserAgentWhenAbsoluteBaseUrl() {
      userAgent.setBaseURL("/path");
      assertThat(userAgent.resolveURI("file.png")).isEqualTo("path/file.png");
   }

   @Test
   void testUserAgentWhenAbsoluteResolveUri() {
      userAgent.setBaseURL("path");
      assertThat(userAgent.resolveURI("/file.png")).isEqualTo("/file.png");
   }

   @Test
   void testUserAgentWhenBothAbsolute() {
      userAgent.setBaseURL("/path");
      assertThat(userAgent.resolveURI("/file.png")).isEqualTo("/file.png");
   }

   @Test
   void testUserAgentWhenTraversalBaseUrl() {
      userAgent.setBaseURL("../path");
      assertThat(userAgent.resolveURI("file.png")).isEqualTo("../path/file.png");
   }

   @Test
   void testUserAgentWhenTraversalResolveUri() {
      userAgent.setBaseURL("path/subpath");
      assertThat(userAgent.resolveURI("../file.png")).isEqualTo("path/file.png");
   }

   @Test
   void testMalFormedUri() {
      userAgent.setBaseURL(".");
      assertThat(userAgent.resolveURI("\\uri")).isNull();
   }

   @Test
   void testNullUri() {
      assertThat(userAgent.resolveURI(null)).isNull();
   }

   @Test
   void testMalFormedBaseUrl() {
      userAgent.setBaseURL("\\.");
      assertThatExceptionOfType(NullPointerException.class)
            .isThrownBy(() -> userAgent.resolveURI("file"))
            .withMessage("Cannot invoke \"String.length()\" because \"this.input\" is null");
   }

   @ParameterizedTest
   @ValueSource(strings = { "foo:file", "file.txt", "\\foo:file.txt" })
   void testResolveAndOpenStream(String uri) {
      userAgent.setBaseURL(".");
      assertThat(userAgent.resolveAndOpenStream(uri)).isNull();
   }
}