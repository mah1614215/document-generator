package com.tinubu.document.generator.flyingsaucer;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.document.generator.core.support.DebugUtils.openDocument;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.ZoneId;
import java.util.Locale;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.document.generator.core.environment.ConfigurableEnvironment.ConfigurableEnvironmentBuilder;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.DocumentGenerator;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory;
import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.XhtmlTemplate;
import com.tinubu.document.generator.core.template.XhtmlTemplate.XhtmlTemplateBuilder;
import com.tinubu.document.generator.flyingsaucer.generator.FlyingSaucerDocumentGenerator.FlyingSaucerDocumentGeneratorBuilder;

class FlyingSaucerDocumentGeneratorTest {

   @Nested
   public class TestSuiteWhenMemoryGeneratedRepository extends TestSuite {

      @BeforeEach
      public void configureGeneratedRepository() {
         GeneratedDocumentRepositoryFactory.memoryDocumentRepositoryFactory();
      }

   }

   @Nested
   public class TestSuiteWhenFsTempGeneratedRepository extends TestSuite {

      @BeforeEach
      public void configureGeneratedRepository() {
         GeneratedDocumentRepositoryFactory.fsTempDocumentRepositoryFactory();
      }

   }

   private abstract class TestSuite {
      @Test
      void generateDocumentWithPdf() {
         DocumentGenerator generator = new FlyingSaucerDocumentGeneratorBuilder(environment()).build();

         try (var pdf = generator
               .generateFromTemplate(template(), TextModel.noopModel(), Skin.noopSkin())
               .loadContent()) {

            assertThat(pdf.document()).isNotNull();
            assertThat(pdf.document().metadata().simpleContentType()).hasValue(APPLICATION_PDF);
            assertThat(pdf.document().content().stringContent()).isNotNull();

            openDocument(pdf);
         }
      }

      @Test
      void convertDocument() {
         DocumentGenerator generator = new FlyingSaucerDocumentGeneratorBuilder().build();
         var document = templateRepository().findDocumentById(DocumentPath.of("template.html")).orElseThrow();
         try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build();
              var convertedDocument = generator.convert(generatedDocument)) {

            assertThat(convertedDocument
                             .document()
                             .metadata().simpleContentType()).hasValue(APPLICATION_PDF);
         }
      }

      @ParameterizedTest
      @MethodSource("com.tinubu.document.generator.flyingsaucer.FlyingSaucerDocumentGeneratorTest#templates")
      void convertDocumentWithWrongContentType_ShouldThrowException(String documentPath, String contentType) {
         DocumentGenerator generator = new FlyingSaucerDocumentGeneratorBuilder(environment()).build();
         var document = templateRepository().findDocumentById(DocumentPath.of(documentPath)).orElseThrow();

         try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build()) {

            assertThatExceptionOfType(DocumentGenerationException.class)
                  .isThrownBy(() -> generator.convert(generatedDocument))
                  .withMessageContaining(String.format(
                        "'template.document.contentType=%s' must be in [application/xhtml+xml,text/html]",
                        contentType));
         }
      }

   }

   private static Stream<Arguments> templates() {
      return Stream.of(arguments("template.ftl", "text/ftl"), arguments("skin/styles.css", "text/css"));
   }

   private static Environment environment() {
      return ConfigurableEnvironmentBuilder
            .ofEnvironment(SystemEnvironment.ofSystemDefaults())
            .timeZone(ZoneId.of("Europe/Paris"))
            .locale(Locale.FRANCE)
            .documentEncoding(StandardCharsets.UTF_8)
            .build();
   }

   private XhtmlTemplate template() {
      return new XhtmlTemplateBuilder()
            .templateRepository(templateRepository())
            .templateId(DocumentPath.of("template.html"))
            .build();
   }

   private static ClasspathDocumentRepository templateRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/flyingsaucer"));
   }
}