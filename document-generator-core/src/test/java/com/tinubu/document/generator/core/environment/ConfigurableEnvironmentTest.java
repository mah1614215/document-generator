package com.tinubu.document.generator.core.environment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Locale.Category;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.document.generator.core.environment.ConfigurableEnvironment.ConfigurableEnvironmentBuilder;

class ConfigurableEnvironmentTest {

   @Test
   void configurableEnvironmentWithSystemDefaultsTest() {
      var configurableEnvironment =
            ConfigurableEnvironmentBuilder.ofEnvironment(SystemEnvironment.ofSystemDefaults()).build();
      assertThat(configurableEnvironment.timeZone()).isEqualTo(ZoneId.systemDefault());
      assertThat(configurableEnvironment.locale()).isEqualTo(Locale.getDefault(Category.FORMAT));
      assertThat(configurableEnvironment.timeFormat()).isEqualTo("HH:mm[:ss[XXX]]");
      assertThat(configurableEnvironment.dateFormat()).isEqualTo("yyyy[-MM[-dd]]");
      assertThat(configurableEnvironment.dateTimeFormat()).isEqualTo("yyyy-MM-dd'T'HH:mm[:ss[XXX]]");
      assertThat(configurableEnvironment.documentEncoding()).isEqualTo(Charset.defaultCharset());
   }

   @Test
   void configurableEnvironmentWithoutRequiredValuesShouldFailTest() {
      final ConfigurableEnvironmentBuilder configurableEnvironmentBuilder =
            new ConfigurableEnvironmentBuilder().dateTimeFormat("date time format").timeFormat("time format");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(configurableEnvironmentBuilder::build)
            .withMessage(
                  "Invariant validation error > Context [ConfigurableEnvironment[timeZone=<null>,locale=<null>,dateFormat=<null>,documentEncoding=<null>]] "
                  + "> {timeZone} 'timeZone' must not be null "
                  + "| {locale} 'locale' must not be null "
                  + "| {dateFormat} 'dateFormat' must not be null "
                  + "| {documentEncoding} 'documentEncoding' must not be null");
   }

   @Test
   void configurableEnvironmentWithAllRequiredValuesShouldPassTest() {
      ConfigurableEnvironment configurableEnvironment = new ConfigurableEnvironmentBuilder()
            .timeZone(ZoneId.of("UTC"))
            .locale(Locale.ENGLISH)
            .dateFormat("yyyyMMdd")
            .documentEncoding(StandardCharsets.UTF_8)
            .build();

      assertThat(configurableEnvironment.timeZone().getId()).isEqualTo("UTC");
      assertThat(configurableEnvironment.locale().getLanguage()).isEqualTo("en");
      assertThat(configurableEnvironment.timeFormat()).isNull();
      assertThat(configurableEnvironment.dateFormat()).isEqualTo("yyyyMMdd");
      assertThat(configurableEnvironment.dateTimeFormat()).isNull();
      assertThat(configurableEnvironment.documentEncoding().displayName()).isEqualTo("UTF-8");

   }
}