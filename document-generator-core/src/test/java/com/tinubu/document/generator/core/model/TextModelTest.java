package com.tinubu.document.generator.core.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.document.generator.core.model.TextModel.TextModelBuilder;

class TextModelTest {

   private static TextModel textModel;

   @BeforeAll
   private static void initData() {
      Map<String, Object> data = new HashMap<>();
      data.put("flag1", Boolean.FALSE);
      data.put("flag2", "not a boolean");
      data.put("flag3", "red");
      data.put("flag4", Boolean.TRUE);
      textModel = new TextModelBuilder().data(data).build();
   }

   @Test
   void noopModelShouldHaveEmptyMapTest() {
      var textModel = TextModel.noopModel();
      assertThat(textModel.data()).isEmpty();
   }

   @Test
   void textModelDataNotFoundShouldReturnEmptyTest() {
      assertThat(textModel.data("key")).isEmpty();
   }

   @Test
   void textModelGetFlag3ShouldReturnRedTest() {
      assertThat(textModel.data("flag3")).hasValue("red");
   }

   @Test
   void textModelRequiredDataNotFoundShouldThrowExceptionTest() {
      assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> textModel.requiredData("key"))
            .withMessage("Unknown 'key' data");
   }

   @Test
   void textModelAddDataTest() {
      var copiedTextModel = TextModelBuilder.from(textModel).addData("key", "value").build();
      assertThat(copiedTextModel.data()).hasSize(5);
      assertThat(copiedTextModel.hasData("key")).isTrue();
   }

   @ParameterizedTest
   @ValueSource(strings = { "flag1", "flag2", "flag3", "flag4" })
   void fromTextModelShouldMatchTheOriginTest(String key) {
      var copiedTextModel = TextModelBuilder.from(textModel).build();
      assertThat(copiedTextModel.data()).isNotEmpty();
      assertThat(copiedTextModel.hasData(key)).isTrue();

   }

   @ParameterizedTest
   @ValueSource(strings = { "flag1", "flag2", "flag3" })
   void textModelHasFlagTest(String flag) {
      assertThat(textModel.hasFlag(flag)).isFalse();
      assertThat(textModel.hasFlag("flag4")).isTrue();
   }

   @ParameterizedTest
   @ValueSource(strings = { "flag 1", "flag 2", "flag 3" })
   void textModelHasDataTest(String key) {
      assertThat(textModel.hasData(key)).isFalse();
      assertThat(textModel.hasData("flag4")).isTrue();
   }

}