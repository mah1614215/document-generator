package com.tinubu.document.generator.core.skin;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;

class NoopSkinTest {

   NoopSkin noopSkin = new NoopSkin();

   @Test
   void skinId() {
      assertThat(noopSkin.skinId()).isEmpty();
   }

   @Test
   void logoId() {
      assertThat(noopSkin.logoId()).isEmpty();
   }

   @Test
   void watermarkId() {
      assertThat(noopSkin.watermarkId()).isEmpty();
   }

   @Test
   void skinRepository() {
      assertThat(noopSkin.skinRepository()).isInstanceOf(NoopDocumentRepository.class);
   }
}