package com.tinubu.document.generator.core.model;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.document.generator.core.model.SpreadsheetModel.SpreadsheetModelBuilder;
import com.tinubu.document.generator.core.model.SpreadsheetModel.Table;

class SpreadsheetModelTest {

   private static SpreadsheetModel spreadsheetModel;

   @BeforeEach
   private void initData() {
      Object[][] defaultDataArray = {
            { "h1", "h2", "h3", "h4" },
            { "r1 c1", "r1 c2", 1, BigDecimal.valueOf(23.32) },
            { "r2 c1", "r2 c2", 2, BigDecimal.valueOf(3.2) },
            { "r3 c1", "r3 c2", 3, BigDecimal.valueOf(4.554) },
            { "r4 c1", "r4 c2", 4, BigDecimal.valueOf(103.3455) } };

      spreadsheetModel =
            new SpreadsheetModelBuilder().tables(map(entry("default", Table.of(defaultDataArray)))).build();
   }

   @Test
   @DisplayName("Noop model should have empty map.")
   void noopModelShouldHaveEmptyMap() {
      var spreadsheetModel = SpreadsheetModel.noopModel();
      assertThat(spreadsheetModel.tables()).isEmpty();
   }

   @Test
   @DisplayName("Should get value for element at given indexes.")
   void getElementValue() {
      var defaultTable = spreadsheetModel.tables().get("default");
      var data = defaultTable.data(1, 3);
      assertThat(data).isNotNull().isEqualTo(BigDecimal.valueOf(23.32));
   }

   @Test
   @DisplayName("Should change element value at given indexes.")
   void changeDataForDimension() {
      var defaultTable = spreadsheetModel.table("default").orElseThrow();
      assertThat(defaultTable.data()[0][0]).isEqualTo("h1");
      var updatedTable = defaultTable.data(0, 0, "Header1");
      assertThat(updatedTable).isNotNull();
      assertThat(defaultTable.rows()).isEqualTo(5);
      assertThat(defaultTable.columns()).isEqualTo(4);
      assertThat(updatedTable.data()[0][0]).isEqualTo("Header1");
   }

   @TestInstance(Lifecycle.PER_CLASS)
   @Nested
   class NestedTableTest {
      @Test
      @DisplayName("Should create table from given list of objects.")
      void createTableFromListOfObject() {
         List<List<Object>> data = data();
         Table createdTable = Table.of(data);
         assertThat(createdTable.rows()).isEqualTo(2);
         assertThat(createdTable.columns()).isEqualTo(4);
         assertThat(createdTable.data()[0][1]).isEqualTo("h2");
         assertThat(createdTable.data()[1][2]).isEqualTo(3);
      }

      @Test
      @DisplayName("Should change row data with given data at given index.")
      void changeRowData() {
         List<List<Object>> data = data();
         Table createdTable = Table.of(data);
         createdTable.rowData(1, Arrays.asList("one", "two"));
         assertThat(createdTable.data()[1][0]).isEqualTo("one");
         assertThat(createdTable.data()[1][1]).isEqualTo("two");
      }

      @Test
      @DisplayName("Should change column data with given data at given index.")
      void changeColumnData() {
         List<List<Object>> data = data();
         Table createdTable = Table.of(data);
         createdTable.columnData(1, Arrays.asList("header2", "22"));
         assertThat(createdTable.data()[0][1]).isEqualTo("header2");
         assertThat(createdTable.data()[1][1]).isEqualTo("22");
      }

      @Test
      @DisplayName("Should copy table content to the existing spreadsheet model.")
      void copySpreadsheetAndAddTableModel() {
         Object[][] array = { { "f1", "f2", "f3", "f4" } };
         var newModel =
               SpreadsheetModelBuilder.from(spreadsheetModel).addTable("footer", Table.of(array)).build();

         assertThat(newModel.tables()).hasSize(2);
         var footer = newModel.tables().get("footer");
         assertThat(footer).isNotNull();
         assertThat(footer.data()).hasDimensions(1, 4);
         assertThat(footer.data(0, 2)).isEqualTo("f3");
      }

      @Test
      @DisplayName("Should replace table content with new array when adding table without name.")
      void addTableToSpreadsheetModel() {
         Object[][] array = { { "1", "2", "3", "4" } };
         var newModel = SpreadsheetModelBuilder.from(spreadsheetModel).addTable(Table.of(array)).build();
         assertThat(newModel.tables()).hasSize(1);
         var defaultTable = newModel.defaultTable().get();
         assertThat(defaultTable).isNotNull();
         assertThat(defaultTable.data()).hasDimensions(1, 4);
         assertThat(defaultTable.data(0, 1)).isEqualTo("2");
      }

      @Test
      @DisplayName("Copied sub-table should have 2x2 dimension.")
      void spreadsheetModelSubTable() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTable(2, 2);
         assertThat(subTable.data()).hasDimensions(2, 2);
         assertThat(subTable.data()[0][0]).isEqualTo("h1");
         assertThat(subTable.data()[0][1]).isEqualTo("h2");
         assertThat(subTable.data()[1][0]).isEqualTo("r1 c1");
         assertThat(subTable.data()[1][1]).isEqualTo("r1 c2");
      }

      @ParameterizedTest
      @ValueSource(ints = { 0, 1, 2, 3, 4 })
      @DisplayName("SubTableRow should copy row at given index.")
      void subTableRowShouldCopyRowAtGivenIndex(int rowIndex) {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableRow(rowIndex);
         assertThat(subTable.data()).hasDimensions(1, 4);
      }

      @Test
      @DisplayName("SubTableRows(0, 2) should copy only 2 first rows.")
      void subTableRowsShouldCopy2FirstRows() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableRows(0, 2);
         assertThat(subTable.data()).hasDimensions(2, 4);
         assertThat(subTable.data()[0][0]).isEqualTo("h1");
         assertThat(subTable.data()[1][1]).isEqualTo("r1 c2");
         assertThat(subTable.data()[1][3]).isEqualTo(BigDecimal.valueOf(23.32));
      }

      @Test
      @DisplayName("SubTableRows(1, 3) should copy 3 rows from row index 1.")
      void shouldCopy3RowsFromIndex1() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableRows(1, 3);
         assertThat(subTable.data()).hasDimensions(3, 4);
         assertThat(subTable.data()[1][3]).isEqualTo(BigDecimal.valueOf(3.2));
      }

      @Test
      @DisplayName("Should throw illegal argument exception on copying row out of array bound.")
      void shouldThrowIllegalArgumentException() {
         var defaultTable = spreadsheetModel.tables().get("default");
         assertThatExceptionOfType(IllegalArgumentException.class)
               .isThrownBy(() -> defaultTable.subTableRows(4, 2))
               .withMessage("'rows' must be included in ]0, 1]");
      }

      @ParameterizedTest
      @DisplayName(
            "Should throw illegal argument exception on creating table with empty row or empty column.")
      @MethodSource("indexes")
      void createTableWithEmptyRowOrColumnShouldThrowException(int rows, int columns, String prefix) {
         assertThatExceptionOfType(IllegalArgumentException.class)
               .isThrownBy(() -> Table.ofEmpty(rows, columns))
               .withMessage(String.format("'%s' must be included in ]0, 16384]", prefix));
      }

      private Stream<Arguments> indexes() {
         return Stream.of(arguments(0, 1, "rows"), arguments(1, 0, "columns"));
      }

      @ParameterizedTest
      @ValueSource(ints = { 0, 1, 2, 3 })
      @DisplayName("SubTableColumn should copy column at given index.")
      void subTableColumnShouldCopyColumnAtGivenIndex(int columnIndex) {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableColumn(columnIndex);
         if (columnIndex == 3) {
            assertThat(subTable.data()[4][0]).isEqualTo(BigDecimal.valueOf(103.3455));
         }
         assertThat(subTable.data()).hasDimensions(5, 1);
      }

      @Test
      @DisplayName("Should copy the first 2 columns.")
      void subTableColumnsShouldCopyFirst2Columns() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableColumns(0, 2);
         assertThat(subTable.data()).hasDimensions(5, 2);
         assertThat(subTable.data()[3][0]).isEqualTo("r3 c1");
         assertThat(subTable.data()[1][1]).isEqualTo("r1 c2");
      }

      @Test
      @DisplayName("Should copy the last 2 columns.")
      void subTableColumnsShouldCopyLast2Columns() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableColumns(2, 2);
         assertThat(subTable.data()).hasDimensions(5, 2);
         assertThat(subTable.data()[2][0]).isEqualTo(2);
         assertThat(subTable.data()[3][1]).isEqualTo(BigDecimal.valueOf(4.554));
      }

      @Test
      @DisplayName("Should copy 3 columns starting from index 1.")
      void subTableColumnsShouldCopy3ColumnsFromIndex1() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableColumns(1, 3);
         assertThat(subTable.data()).hasDimensions(5, 3);
         assertThat(subTable.data()[0][2]).isEqualTo("h4");
         assertThat(subTable.data()[1][2]).isEqualTo(BigDecimal.valueOf(23.32));
         assertThat(subTable.data()[4][0]).isEqualTo("r4 c2");
      }

      @Test
      @DisplayName("Call to sub table element method should return element at coordinates (1, 3).")
      void subTableElementShouldReturnElement() {
         var defaultTable = spreadsheetModel.tables().get("default");
         var subTable = defaultTable.subTableElement(1, 3);
         assertThat(subTable.data()).hasDimensions(1, 1);
         assertThat(subTable.data()[0][0]).isEqualTo(BigDecimal.valueOf(23.32));
      }

      private List<List<Object>> data() {
         Object[][] array = {
               { "h1", "h2", "h3", "h4" }, { 1, 2, 3, 4 } };
         return Arrays.stream(array).map(Arrays::asList).collect(Collectors.toList());
      }

   }

}