package com.tinubu.document.generator.core.environment;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.Charset;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Locale.Category;

import org.junit.jupiter.api.Test;

class SystemEnvironmentTest {

   @Test
   void systemDefaultsTest() {
      SystemEnvironment systemDefaults = SystemEnvironment.ofSystemDefaults();
      assertThat(systemDefaults.timeZone()).isEqualTo(ZoneId.systemDefault());
      assertThat(systemDefaults.locale()).isEqualTo(Locale.getDefault(Category.FORMAT));
      assertThat(systemDefaults.timeFormat()).isEqualTo("HH:mm[:ss[XXX]]");
      assertThat(systemDefaults.dateFormat()).isEqualTo("yyyy[-MM[-dd]]");
      assertThat(systemDefaults.dateTimeFormat()).isEqualTo("yyyy-MM-dd'T'HH:mm[:ss[XXX]]");
      assertThat(systemDefaults.documentEncoding()).isEqualTo(Charset.defaultCharset());
   }

}