package com.tinubu.document.generator.core.environment;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.nio.charset.Charset;
import java.time.ZoneId;
import java.util.Locale;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;

/**
 * Represents the general settings used to generate documents.
 */
public interface Environment extends Value {

   /**
    * Returns a time-zone ID (such as Europe/Paris) used to convert formatted dates.
    *
    * @return time-zone
    */
   ZoneId timeZone();

   /**
    * Returns the general locale.
    *
    * @return locale
    */
   Locale locale();

   /**
    * Returns the general date format. Format can vary depending on generator.
    *
    * @return date formatter
    */
   String dateFormat();

   /**
    * Returns the general time format. Format can vary depending on generator.
    *
    * @return date formatter
    */
   String timeFormat();

   /**
    * Returns the general date format. Format can vary depending on generator.
    *
    * @return date formatter
    */
   String dateTimeFormat();

   /**
    * Returns the general encoding for generated documents.
    *
    * @return document encoding
    */
   Charset documentEncoding();

   default Fields<? extends Environment> defineDomainFields() {
      return Fields
            .<Environment>builder()
            .field("timeZone", Environment::timeZone, isNotNull())
            .field("locale", Environment::locale, isNotNull())
            .field("dateFormat", Environment::dateFormat, isNotNull())
            .field("documentEncoding", Environment::documentEncoding, isNotNull())
            .build();
   }
}
