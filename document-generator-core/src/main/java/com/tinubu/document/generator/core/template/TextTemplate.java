package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.instance;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;

/**
 * Generic template for text-based documents.
 */
public class TextTemplate extends AbstractValue implements Template {

   protected final DocumentPath templateId;
   protected final DocumentRepository templateRepository;
   protected final DocumentPath headerId;
   protected final DocumentPath footerId;

   protected TextTemplate(TextTemplateBuilder builder) {
      this.templateId = builder.templateId;
      this.templateRepository = builder.templateRepository;
      this.headerId = builder.headerId;
      this.footerId = builder.footerId;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends TextTemplate> defineDomainFields() {
      return Fields
            .<TextTemplate>builder()
            .superFields((Fields<TextTemplate>) Template.super.defineDomainFields())
            .field("headerId", v -> v.headerId)
            .field("footerId", v -> v.footerId)
            .build();
   }

   @Getter
   @Override
   public DocumentPath templateId() {
      return templateId;
   }

   @Getter
   @Override
   public DocumentRepository templateRepository() {
      return templateRepository;
   }

   /**
    * Returns the optional header document id.
    *
    * @return header document id or {@link Optional#empty()}
    */
   @Getter
   public Optional<DocumentPath> headerId() {
      return nullable(headerId);
   }

   /**
    * Returns the optional footer document id.
    *
    * @return footer document id or {@link Optional#empty()}
    */
   @Getter
   public Optional<DocumentPath> footerId() {
      return nullable(footerId);
   }

   public static TextTemplateBuilder reconstituteBuilder() {
      return new TextTemplateBuilder().reconstitute();
   }

   public static class TextTemplateBuilder extends DomainBuilder<TextTemplate> {
      protected DocumentRepository rawContentLayer;
      protected DocumentRepository templateRepository;
      protected DocumentPath templateId;
      protected DocumentPath headerId;
      protected DocumentPath footerId;

      public static TextTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new TextTemplateBuilder()
               .templateId(template.templateId())
               .templateRepository(template.templateRepository());
      }

      @SuppressWarnings("resource")
      public static TextTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return TextTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Getter
      public DocumentRepository templateRepository() {
         return templateRepository;
      }

      @Setter
      public TextTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         this.templateRepository = templateRepository;
         return this;
      }

      @Getter
      public DocumentPath templateId() {
         return templateId;
      }

      @Setter
      public TextTemplateBuilder templateId(DocumentPath templateId) {
         this.templateId = templateId;
         return this;
      }

      public TextTemplateBuilder templateContent(String templateContent) {
         this.rawContentLayer =
               nullable(rawContentLayer, () -> instance().createDocumentRepository("text-template-content"));
         Document rawContentDocument = rawContentDocument("template", templateContent);
         this.rawContentLayer.saveDocument(rawContentDocument, false).orElseThrow();
         this.templateId = rawContentDocument.documentId();
         return this;
      }

      @Getter
      public DocumentPath headerId() {
         return headerId;
      }

      @Setter
      public TextTemplateBuilder headerId(DocumentPath headerId) {
         this.headerId = headerId;
         return this;
      }

      public TextTemplateBuilder headerContent(String headerContent) {
         this.rawContentLayer = nullable(rawContentLayer,
                                         () -> instance().createDocumentRepository(
                                               "text-template-header-content"));
         Document rawContentDocument = rawContentDocument("header", headerContent);
         this.rawContentLayer.saveDocument(rawContentDocument, false).orElseThrow();
         this.headerId = rawContentDocument.documentId();
         return this;
      }

      @Getter
      public DocumentPath footerId() {
         return footerId;
      }

      @Setter
      public TextTemplateBuilder footerId(DocumentPath footerId) {
         this.footerId = footerId;
         return this;
      }

      public TextTemplateBuilder footerContent(String footerContent) {
         this.rawContentLayer = nullable(rawContentLayer,
                                         () -> instance().createDocumentRepository(
                                               "text-template-footer-content"));
         Document rawContentDocument = rawContentDocument("footer", footerContent);
         this.rawContentLayer.saveDocument(rawContentDocument, false).orElseThrow();
         this.footerId = rawContentDocument.documentId();
         return this;
      }

      @Override
      @SuppressWarnings("unchecked")
      public TextTemplateBuilder finalizeBuild() {
         if (rawContentLayer != null) {
            this.templateRepository = nullable(templateRepository,
                                               r -> r.stackRepository(rawContentLayer, true),
                                               rawContentLayer);
         }
         return this;
      }

      @Override
      public TextTemplate buildDomainObject() {
         return new TextTemplate(this);
      }

      private Document rawContentDocument(String name, String rawContent) {
         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(name + "-" + Uuid
                                          .newNameBasedSha1Uuid(name)
                                          .stringValue()))
                                    .contentType(TEXT_PLAIN)
                                    .streamContent(rawContent, StandardCharsets.UTF_8)
                                    .build());
      }

   }
}
