package com.tinubu.document.generator.core.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.type;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.isEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentEntryRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;
import static java.util.Objects.nonNull;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.rules.BaseRules;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Skin definitions for document generation.
 */
public interface Skin extends Value {

   /**
    * No-op {@link Skin} instance.
    *
    * @return no-op skin
    */
   static NoopSkin noopSkin() {
      return new NoopSkin();
   }

   /**
    * Returns the optional skin document identifier.
    *
    * @return document identifier
    */
   Optional<DocumentPath> skinId();

   /**
    * Returns the optional logo document identifier.
    *
    * @return document identifier
    */
   Optional<DocumentPath> logoId();

   /**
    * Returns the optional watermark document identifier.
    *
    * @return document identifier
    */
   Optional<DocumentPath> watermarkId();

   /**
    * Returns the skin document repository. This repository is used to retrieve the skin itself, but
    * also some extra resources depending on skin implementation.
    *
    * @return skin documents repository
    */
   DocumentRepository skinRepository();

   /**
    * Returns the skin document if any.
    *
    * @return skin document or {@link Optional#empty}
    */
   default Optional<Document> skinDocument() {
      return skinId().flatMap(skinId -> skinRepository().findDocumentById(skinId));
   }

   /**
    * Returns the skin document entry if any.
    *
    * @return skin document entry or {@link Optional#empty}
    */
   default Optional<DocumentEntry> skinDocumentEntry() {
      return skinId().flatMap(skinId -> skinRepository().findDocumentEntryById(skinId));
   }

   /**
    * Returns the logo document if any.
    *
    * @return logo document or {@link Optional#empty}
    */
   default Optional<Document> logoDocument() {
      return logoId().flatMap(logoId -> skinRepository().findDocumentById(logoId));
   }

   /**
    * Returns the logo document entry if any.
    *
    * @return logo document entry or {@link Optional#empty}
    */
   default Optional<DocumentEntry> logoDocumentEntry() {
      return logoId().flatMap(logoId -> skinRepository().findDocumentEntryById(logoId));
   }

   /**
    * Returns the watermark document if any.
    *
    * @return logo document or {@link Optional#empty}
    */
   default Optional<Document> watermarkDocument() {
      return watermarkId().flatMap(watermarkId -> skinRepository().findDocumentById(watermarkId));
   }

   /**
    * Returns the watermark document entry if any.
    *
    * @return watermark document entry or {@link Optional#empty}
    */
   default Optional<DocumentEntry> watermarkDocumentEntry() {
      return watermarkId().flatMap(watermarkId -> skinRepository().findDocumentEntryById(watermarkId));
   }

   default Fields<? extends Skin> defineDomainFields() {
      return Fields
            .<Skin>builder()
            .field("skinId", Skin::skinId)
            .field("logoId", Skin::logoId)
            .field("watermarkId", Skin::watermarkId)
            .field("skinRepository", Skin::skinRepository, isNotNull())
            .invariants(isValidSkin(), isValidLogo())
            .build();
   }

   default Invariant<Skin> isValidSkin() {
      InvariantRule<Optional<DocumentEntry>> isValidSkinDocumentEntry =
            isEmpty().orValue(optionalValue(metadata(contentType(isSupportedSkinContentType()))));

      return Invariant.of(() -> this,
                          BaseRules
                                .<Skin, Optional<DocumentEntry>>property(skin -> skin
                                      .skinId()
                                      .map(skinId -> skin.skinRepository().findDocumentEntryById(skinId))
                                      .orElse(optional()), "skin.document", isValidSkinDocumentEntry)
                                .ifValue(skin -> nonNull(skin.skinRepository())));
   }

   default Invariant<Skin> isValidLogo() {
      InvariantRule<Optional<DocumentEntry>> isValidLogoDocumentEntry =
            isEmpty().orValue(optionalValue(metadata(contentType(type(isEqualTo(value("image")))))));

      return Invariant.of(() -> this,
                          BaseRules
                                .<Skin, Optional<DocumentEntry>>property(skin -> skin
                                      .logoId()
                                      .map(logoId -> skin.skinRepository().findDocumentEntryById(logoId))
                                      .orElse(optional()), "logo.document", isValidLogoDocumentEntry)
                                .ifValue(skin -> nonNull(skin.skinRepository())));
   }

   default InvariantRule<MimeType> isSupportedSkinContentType() {
      return isNotNull();
   }

}
