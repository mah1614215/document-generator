package com.tinubu.document.generator.core.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.instance;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;

public class CssSkin extends AbstractValue implements Skin {
   protected final DocumentPath skinId;
   protected final DocumentRepository skinRepository;
   protected final DocumentPath logoId;
   protected final DocumentPath watermarkId;

   protected CssSkin(CssSkinBuilder builder) {
      this.skinId = builder.skinId;
      this.skinRepository = builder.skinRepository;
      this.logoId = builder.logoId;
      this.watermarkId = builder.watermarkId;
   }

   @SuppressWarnings("unchecked")
   public Fields<? extends CssSkin> defineDomainFields() {
      return Fields.<CssSkin>builder().superFields((Fields<CssSkin>) Skin.super.defineDomainFields()).build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedSkinContentType() {
      return isTypeAndSubtypeEqualTo(value(TEXT_CSS));
   }

   @Getter
   @Override
   public Optional<DocumentPath> skinId() {
      return nullable(skinId);
   }

   @Getter
   @Override
   public DocumentRepository skinRepository() {
      return skinRepository;
   }

   @Getter
   public Optional<DocumentPath> logoId() {
      return nullable(logoId);
   }

   @Getter
   public Optional<DocumentPath> watermarkId() {
      return nullable(watermarkId);
   }

   /**
    * No-op {@link CssSkin} instance.
    *
    * @return no-op skin
    */
   public static CssSkin noopSkin() {
      class NoopSkin extends CssSkin {
         protected NoopSkin() {
            super(new CssSkinBuilder().skinRepository(new NoopDocumentRepository()).finalizeBuild());
         }
      }

      return new NoopSkin();
   }

   public static CssSkinBuilder reconstituteBuilder() {
      return new CssSkinBuilder().reconstitute();
   }

   public static class CssSkinBuilder extends DomainBuilder<CssSkin> {
      protected DocumentRepository rawContentLayer;
      protected DocumentRepository skinRepository;
      protected DocumentPath skinId;
      protected DocumentPath logoId;
      protected DocumentPath watermarkId;

      @Setter
      public CssSkinBuilder skinRepository(DocumentRepository skinRepository) {
         this.skinRepository = skinRepository;
         return this;
      }

      @Setter
      public CssSkinBuilder skinId(DocumentPath skinId) {
         this.skinId = skinId;
         return this;
      }

      public CssSkinBuilder skinContent(String skinContent) {
         this.rawContentLayer =
               nullable(rawContentLayer, () -> instance().createDocumentRepository("css-skin-content"));
         Document rawContentDocument = rawContentDocument("skin", skinContent);
         this.rawContentLayer.saveDocument(rawContentDocument, false).orElseThrow();
         this.skinId = rawContentDocument.documentId();
         return this;
      }

      @Setter
      public CssSkinBuilder logoId(DocumentPath logoId) {
         this.logoId = logoId;
         return this;
      }

      @Setter
      public CssSkinBuilder watermarkId(DocumentPath watermarkId) {
         this.watermarkId = watermarkId;
         return this;
      }

      @Override
      @SuppressWarnings("unchecked")
      public CssSkinBuilder finalizeBuild() {
         if (rawContentLayer != null) {
            this.skinRepository =
                  nullable(skinRepository, r -> r.stackRepository(rawContentLayer, true), rawContentLayer);
         }
         return this;
      }

      @Override
      public CssSkin buildDomainObject() {
         return new CssSkin(this);
      }

      private Document rawContentDocument(String name, String rawContent) {
         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(Uuid
                                                                      .newNameBasedSha1Uuid(name)
                                                                      .stringValue()))
                                    .streamContent(rawContent, StandardCharsets.UTF_8)
                                    .contentType(TEXT_CSS)
                                    .build());
      }

   }
}
