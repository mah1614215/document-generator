package com.tinubu.document.generator.core.generator;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;
import com.tinubu.document.generator.core.template.Template;

/**
 * A generated document is a generic representation of a document with external references.
 * External references should be relative paths, accessible from specified document repository.
 * <p>
 * Document and document repository are closed with this {@link GeneratedDocument#close() AutoCloseable}.
 */
public class GeneratedDocument extends AbstractValue implements AutoCloseable {
   protected final Document document;
   protected final DocumentRepository documentRepository;

   protected GeneratedDocument(GeneratedDocumentBuilder builder) {
      this.document = builder.document;
      this.documentRepository = nullable(builder.documentRepository, NoopDocumentRepository::new);
   }

   @Override
   public Fields<? extends GeneratedDocument> defineDomainFields() {
      return Fields
            .<GeneratedDocument>builder()
            .field("document", v -> v.document, isNotNull())
            .field("documentRepository", v -> v.documentRepository, isNotNull())
            .build();
   }

   /**
    * A document with external references.
    *
    * @return document
    */
   @Getter
   public Document document() {
      return document;
   }

   /**
    * A document repository from which to retrieve external references.
    *
    * @return document repository
    */
   @Getter
   public DocumentRepository documentRepository() {
      return documentRepository;
   }

   /**
    * Returns a new generated document with document content loaded into memory.
    *
    * @return new generated document with loaded content
    */
   public GeneratedDocument loadContent() {
      return GeneratedDocumentBuilder.from(this).document(document.loadContent()).build();
   }

   /**
    * Represents this generated document as a new {@link Template}, for chained generators.
    *
    * @return this generated document as a template
    */
   public Template asTemplate() {
      return new Template() {
         @Override
         public DocumentPath templateId() {
            return document.documentId();
         }

         @Override
         public DocumentRepository templateRepository() {
            DocumentRepository templateRepository =
                  documentRepository.stackRepository(GeneratedDocumentRepositoryFactory
                                                           .instance()
                                                           .createDocumentRepository("template-from-generated"),
                                                     true);
            templateRepository.saveDocument(document, true).orElseThrow();

            return templateRepository;
         }
      };
   }

   /**
    * Converts this generated document using another generator.
    *
    * @param documentGenerator generator
    *
    * @return converted generated document
    */
   public GeneratedDocument convert(DocumentGenerator documentGenerator) {
      return documentGenerator.convert(this);
   }

   public static GeneratedDocumentBuilder reconstituteBuilder() {
      return new GeneratedDocumentBuilder().reconstitute();
   }

   @Override
   public void close() {
      try {
         smartFinalize(() -> document.content().close(), documentRepository::close);
      } catch (Exception e) {
         throw sneakyThrow(e);
      }
   }

   public static class GeneratedDocumentBuilder extends DomainBuilder<GeneratedDocument> {
      private Document document;
      private DocumentRepository documentRepository;

      public static GeneratedDocumentBuilder from(GeneratedDocument generatedDocument) {
         return new GeneratedDocumentBuilder()
               .<GeneratedDocumentBuilder>reconstitute()
               .document(generatedDocument.document)
               .documentRepository(generatedDocument.documentRepository);
      }

      @Setter
      public GeneratedDocumentBuilder document(Document document) {
         this.document = document;
         return this;
      }

      @Setter
      public GeneratedDocumentBuilder documentRepository(DocumentRepository documentRepository) {
         this.documentRepository = documentRepository;
         return this;
      }

      @Override
      public GeneratedDocument buildDomainObject() {
         return new GeneratedDocument(this);
      }
   }
}
