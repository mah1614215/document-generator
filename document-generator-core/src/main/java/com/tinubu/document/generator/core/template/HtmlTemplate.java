package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.document.generator.core.template.HtmlNormalizer.normalizeHtml;
import static java.util.Objects.nonNull;

import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory;

public class HtmlTemplate extends TextTemplate {

   protected HtmlTemplate(HtmlTemplateBuilder builder, UnaryOperator<Document> documentNormalizer) {
      super(normalizeTemplate(builder, documentNormalizer));
   }

   protected HtmlTemplate(HtmlTemplateBuilder builder) {
      super(normalizeTemplate(builder, templateDocument -> normalizeHtml(templateDocument, false)));
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends HtmlTemplate> defineDomainFields() {
      return Fields
            .<HtmlTemplate>builder()
            .superFields((Fields<HtmlTemplate>) super.defineDomainFields())
            .build();
   }

   protected static HtmlTemplateBuilder normalizeTemplate(HtmlTemplateBuilder builder,
                                                          UnaryOperator<Document> templateNormalizer) {
      validate(builder, "builder", isNotNull()).orThrow();
      validate(templateNormalizer, "templateNormalizer", isNotNull()).orThrow();

      if (nonNull(builder.templateRepository) && nonNull(builder.templateId)) {
         builder.templateRepository.findDocumentById(builder.templateId).ifPresent(templateDocument -> {
            builder.templateRepository(builder.templateRepository.stackRepository(
                  GeneratedDocumentRepositoryFactory
                        .instance()
                        .createDocumentRepository("html-normalize-template"),
                  true));
            builder.templateRepository
                  .saveDocument(templateNormalizer.apply(templateDocument), true)
                  .orElseThrow();
         });
      }

      return builder;
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return isTypeAndSubtypeEqualTo(value(TEXT_HTML));
   }

   public static HtmlTemplateBuilder reconstituteBuilder() {
      return new HtmlTemplateBuilder().reconstitute();
   }

   public static class HtmlTemplateBuilder extends TextTemplateBuilder {

      public static HtmlTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new HtmlTemplateBuilder()
               .templateId(template.templateId())
               .templateRepository(template.templateRepository());
      }

      @SuppressWarnings("resource")
      public static HtmlTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return HtmlTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      @Setter
      public HtmlTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (HtmlTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder templateId(DocumentPath templateId) {
         return (HtmlTemplateBuilder) super.templateId(templateId);
      }

      @Override
      public HtmlTemplateBuilder templateContent(String templateContent) {
         return (HtmlTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder headerId(DocumentPath headerId) {
         return (HtmlTemplateBuilder) super.headerId(headerId);
      }

      @Override
      public HtmlTemplateBuilder headerContent(String header) {
         return (HtmlTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder footerId(DocumentPath footerId) {
         return (HtmlTemplateBuilder) super.footerId(footerId);
      }

      @Override
      public HtmlTemplateBuilder footerContent(String footer) {
         return (HtmlTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public HtmlTemplate buildDomainObject() {
         return new HtmlTemplate(this);
      }

      @Override
      public HtmlTemplate build() {
         return (HtmlTemplate) super.build();
      }

   }
}
