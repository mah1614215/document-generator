package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.isPresent;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentEntryRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Template representation for generators.
 */
public interface Template extends Value {

   /**
    * Returns the template document identifier.
    *
    * @return document identifier
    */
   DocumentPath templateId();

   /**
    * Returns the template document repository. This repository is used to retrieve the template itself, but
    * also some extra resources depending on template implementation.
    *
    * @return document identifier
    */
   DocumentRepository templateRepository();

   /**
    * Returns the template document if any.
    *
    * @return template document or {@link Optional#empty}
    */
   default Document templateDocument() {
      return templateRepository()
            .findDocumentById(templateId())
            .orElseThrow(() -> new IllegalStateException(String.format("Unknown '%s' template document",
                                                                       templateId().stringValue())));
   }

   /**
    * Returns the template document entry if any.
    *
    * @return template document entry or {@link Optional#empty}
    */
   default DocumentEntry templateDocumentEntry() {
      return templateRepository()
            .findDocumentEntryById(templateId())
            .orElseThrow(() -> new IllegalStateException(String.format("Unknown '%s' template document",
                                                                       templateId().stringValue())));
   }

   default Fields<? extends Template> defineDomainFields() {
      return Fields
            .<Template>builder()
            .field("templateId", Template::templateId, isNotNull())
            .field("templateRepository", Template::templateRepository, isNotNull())
            .invariants(isValidTemplate())
            .build();
   }

   default Invariant<? extends Template> isValidTemplate() {
      return Invariant.of(() -> this,
                          property(template -> template
                                         .templateRepository()
                                         .findDocumentEntryById(template.templateId()),
                                   "template.document",
                                   isPresent().andValue(optionalValue(metadata(contentType(
                                         isSupportedTemplateContentType()))))));
   }

   default InvariantRule<MimeType> isSupportedTemplateContentType() {
      return isNotNull();
   }

}
