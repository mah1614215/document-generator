package com.tinubu.document.generator.core.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.common.ContentTypeDocumentRenamer;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;

/**
 * {@link DocumentRepository} generator factory, used when a writable repository is required to store
 * internally generated documents.
 * Generated repositories can be for example {@link MemoryDocumentRepository} or
 * {@link FsDocumentRepository}.
 * Generated documents must auto-cleanup on {@link DocumentRepository#close()}.
 * <p>
 * By default, created repositories are instance of {@link MemoryDocumentRepository}, you can use
 * {@link #documentRepositoryFactory(Function)} to change defaults.
 */
public class GeneratedDocumentRepositoryFactory {

   private static final ContentTypeDocumentRenamer CONTENT_TYPE_RENAMER_PROCESSOR =
         new ContentTypeDocumentRenamer(false, true);

   private static final Function<String, DocumentRepository> DEFAULT_DOCUMENT_REPOSITORY_FACTORY =
         GeneratedDocumentRepositoryFactory::memoryDocumentRepositoryFactory;

   private static Function<String, DocumentRepository> documentRepositoryFactory =
         DEFAULT_DOCUMENT_REPOSITORY_FACTORY;

   private static class SingletonHolder {
      private static final GeneratedDocumentRepositoryFactory INSTANCE =
            new GeneratedDocumentRepositoryFactory();
   }

   /**
    * Returns factory singleton instance.
    *
    * @return factory singleton instance
    */
   public static GeneratedDocumentRepositoryFactory instance() {
      return SingletonHolder.INSTANCE;
   }

   /**
    * In-memory document repository factory.
    *
    * @param qualifier storage qualifier
    *
    * @return new document repository
    *
    * @see GeneratedDocumentRepositoryFactory#documentRepositoryFactory(Function)
    */
   public static DocumentRepository memoryDocumentRepositoryFactory(String qualifier) {
      return new MemoryDocumentRepository();
   }

   /**
    * Filesystem-based document repository factory to store large documents and free memory.
    * Documents are stored into system temporary directories.
    *
    * @param qualifier storage qualifier
    *
    * @return new document repository
    *
    * @see GeneratedDocumentRepositoryFactory#documentRepositoryFactory(Function)
    */
   public static DocumentRepository fsTempDocumentRepositoryFactory(String qualifier) {
      try {
         Path tempDirectory = Files.createTempDirectory(qualifier);

         return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                               .storagePath(tempDirectory)
                                               .storageStrategy(DirectFsStorageStrategy.class)
                                               .failFastIfMissingStoragePath(true)
                                               .deleteRepositoryOnClose(true)
                                               .build());
      } catch (IOException e) {
         throw sneakyThrow(e);
      }
   }

   /**
    * Globally configures a new document repository factory for generated documents.
    *
    * @param documentRepositoryFactory factory to set
    */
   public synchronized static void documentRepositoryFactory(Function<String, DocumentRepository> documentRepositoryFactory) {
      validate(documentRepositoryFactory, "documentRepositoryFactory", isNotNull()).orThrow();

      GeneratedDocumentRepositoryFactory.documentRepositoryFactory = documentRepositoryFactory;
   }

   /**
    * Globally configures a new document repository factory for generated documents, using an in-memory
    * document repository.
    */
   public synchronized static void memoryDocumentRepositoryFactory() {
      documentRepositoryFactory(GeneratedDocumentRepositoryFactory::memoryDocumentRepositoryFactory);
   }

   /**
    * Globally configures a new document repository factory for generated documents, using a filesystem
    * document repository that use system temporary directories.
    */
   public synchronized static void fsTempDocumentRepositoryFactory() {
      documentRepositoryFactory(GeneratedDocumentRepositoryFactory::fsTempDocumentRepositoryFactory);
   }

   /**
    * Creates a new writable repository instance to store generated documents. Each generated repository must
    * point to unique storage location.
    *
    * @param qualifier storage qualifier
    *
    * @return new document repository
    */
   public DocumentRepository createDocumentRepository(String qualifier) {
      validate(qualifier, "qualifier", isNotBlank()).orThrow();

      return documentRepositoryFactory.apply(qualifier);
   }

   public static Document autoExtension(Document document) {
      return document.process(CONTENT_TYPE_RENAMER_PROCESSOR);
   }
}
