package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;

public class SpreadsheetTemplate extends AbstractValue implements Template {

   protected final DocumentPath templateId;
   protected final DocumentRepository templateRepository;

   protected SpreadsheetTemplate(SpreadsheetTemplateBuilder builder) {
      this.templateId = builder.templateId;
      this.templateRepository = builder.templateRepository;
   }

   @Getter
   @Override
   public DocumentPath templateId() {
      return templateId;
   }

   @Getter
   @Override
   public DocumentRepository templateRepository() {
      return templateRepository;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends SpreadsheetTemplate> defineDomainFields() {
      return Fields
            .<SpreadsheetTemplate>builder()
            .superFields((Fields<SpreadsheetTemplate>) Template.super.defineDomainFields())
            .field("templateId", v -> v.templateId)
            .build();
   }

   public static class SpreadsheetTemplateBuilder extends DomainBuilder<SpreadsheetTemplate> {
      protected DocumentPath templateId;
      protected DocumentRepository templateRepository;

      public static SpreadsheetTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new SpreadsheetTemplateBuilder()
               .templateId(template.templateId())
               .templateRepository(template.templateRepository());
      }

      @SuppressWarnings("resource")
      public static SpreadsheetTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return SpreadsheetTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Setter
      public SpreadsheetTemplateBuilder templateId(DocumentPath templateId) {
         this.templateId = templateId;
         return this;
      }

      @Setter
      public SpreadsheetTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         this.templateRepository = templateRepository;
         return this;
      }

      @Override
      public SpreadsheetTemplate buildDomainObject() {
         return new SpreadsheetTemplate(this);
      }

   }
}
