package com.tinubu.document.generator.core.generator;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.document.generator.core.model.Model;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.Template;

/**
 * This framework is used to generate documents based on various {@link Template templates},
 * {@link Model modes} and {@link Skin skins}.
 */
public interface DocumentGenerator {

   /**
    * Generates document.
    *
    * @param template template used to generate the document
    * @param model data model
    * @param skin document skin
    *
    * @return generated document
    *
    * @throws InvariantValidationException if bad parameters are detected in function usage
    * @throws DocumentGenerationException if any error occurs while generating the document
    */
   GeneratedDocument generateFromTemplate(Template template, Model model, Skin skin);

   /**
    * Converts document from a generated document.
    * Can be used to chain generators, no template, model or skin is configurable.
    *
    * @param generatedDocument generated document
    *
    * @return generated document
    *
    * @throws InvariantValidationException if bad parameters are detected in function usage
    * @throws DocumentGenerationException if any error occurs while generating the document
    */
   GeneratedDocument convert(GeneratedDocument generatedDocument);

}
