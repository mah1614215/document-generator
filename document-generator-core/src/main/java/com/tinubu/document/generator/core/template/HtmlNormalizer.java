package com.tinubu.document.generator.core.template;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.tidy.Tidy;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;

public final class HtmlNormalizer {

   private static final Logger log = LoggerFactory.getLogger(HtmlNormalizer.class);

   private HtmlNormalizer() {
   }

   /**
    * Creates a new document from specified document with normalized HTML content.
    *
    * @param html HTML document
    * @param xhtml whether to convert HTML to XHTML
    *
    * @return normalized HTML document
    */
   public static Document normalizeHtml(Document html, boolean xhtml) {
      Charset templateEncoding = html.metadata().contentEncoding().orElse(StandardCharsets.UTF_8);

      try (ByteArrayOutputStream tidyErrorStream = new ByteArrayOutputStream();
           PrintWriter tidyErrorWriter = new PrintWriter(tidyErrorStream)) {
         Tidy tidy = new Tidy();

         tidy.setShowWarnings(false);
         tidy.setShowErrors(10);
         tidy.setInputEncoding(templateEncoding.name());
         tidy.setOutputEncoding(templateEncoding.name());
         tidy.setXHTML(xhtml);
         tidy.setErrout(tidyErrorWriter);

         try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            org.w3c.dom.Document dom = tidy.parseDOM(html.content().readerContent(templateEncoding), null);
            tidy.pprint(dom, outputStream);

            byte[] htmlContent = outputStream.toByteArray();
            return html.content(new InputStreamDocumentContentBuilder()
                                      .content(htmlContent, templateEncoding)
                                      .build());
         } finally {
            tidyErrorWriter.flush();
            String tidyErrors = tidyErrorStream.toString().trim();
            if (!tidyErrors.isEmpty() && log.isDebugEnabled()) {
               log.debug("Tidy : '{}'", tidyErrors.replaceAll("\n|\r\n|\r", " | "));
            }
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

}
