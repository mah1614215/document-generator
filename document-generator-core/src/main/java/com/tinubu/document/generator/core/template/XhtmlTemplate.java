package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.document.generator.core.template.HtmlNormalizer.normalizeHtml;

import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;

public class XhtmlTemplate extends HtmlTemplate {

   protected XhtmlTemplate(XhtmlTemplateBuilder builder, UnaryOperator<Document> documentNormalizer) {
      super(builder, documentNormalizer);
   }

   protected XhtmlTemplate(XhtmlTemplateBuilder builder) {
      super(builder, templateDocument -> normalizeHtml(templateDocument, true));
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends XhtmlTemplate> defineDomainFields() {
      return Fields
            .<XhtmlTemplate>builder()
            .superFields((Fields<XhtmlTemplate>) super.defineDomainFields())
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return withStrippedParameters(isIn(value(list(APPLICATION_XHTML, TEXT_HTML))));
   }

   public static XhtmlTemplateBuilder reconstituteBuilder() {
      return new XhtmlTemplateBuilder().reconstitute();
   }

   public static class XhtmlTemplateBuilder extends HtmlTemplateBuilder {

      public static XhtmlTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new XhtmlTemplateBuilder()
               .templateId(template.templateId())
               .templateRepository(template.templateRepository());
      }

      @SuppressWarnings("resource")
      public static XhtmlTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return XhtmlTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (XhtmlTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder templateId(DocumentPath templateId) {
         return (XhtmlTemplateBuilder) super.templateId(templateId);
      }

      @Override
      public XhtmlTemplateBuilder templateContent(String templateContent) {
         return (XhtmlTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder headerId(DocumentPath headerId) {
         return (XhtmlTemplateBuilder) super.headerId(headerId);
      }

      @Override
      public XhtmlTemplateBuilder headerContent(String header) {
         return (XhtmlTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder footerId(DocumentPath footerId) {
         return (XhtmlTemplateBuilder) super.footerId(footerId);
      }

      @Override
      public XhtmlTemplateBuilder footerContent(String footer) {
         return (XhtmlTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public XhtmlTemplate buildDomainObject() {
         return new XhtmlTemplate(this);
      }

      @Override
      public XhtmlTemplate build() {
         return (XhtmlTemplate) super.build();
      }

   }
}
