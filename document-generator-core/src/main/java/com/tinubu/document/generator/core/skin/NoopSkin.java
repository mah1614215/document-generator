package com.tinubu.document.generator.core.skin;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Optional;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;

/**
 * Generic no-op skin definition.
 */
public class NoopSkin implements Skin {

   @Override
   public Optional<DocumentPath> skinId() {
      return optional();
   }

   @Override
   public Optional<DocumentPath> logoId() {
      return optional();
   }

   @Override
   public Optional<DocumentPath> watermarkId() {
      return optional();
   }

   @Override
   public DocumentRepository skinRepository() {
      return new NoopDocumentRepository();
   }

}
