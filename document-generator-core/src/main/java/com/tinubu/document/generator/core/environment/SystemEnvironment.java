package com.tinubu.document.generator.core.environment;

import java.nio.charset.Charset;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Locale.Category;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;

/**
 * Environment with default values from system.
 */
public class SystemEnvironment extends AbstractValue implements Environment {

   protected final ZoneId timeZone;
   protected final Locale locale;
   protected final String dateFormat;
   protected final String timeFormat;
   protected final String dateTimeFormat;
   protected final Charset documentEncoding;

   protected SystemEnvironment() {
      this.timeZone = ZoneId.systemDefault();
      this.locale = Locale.getDefault(Category.FORMAT);
      this.dateFormat = "yyyy[-MM[-dd]]";
      this.timeFormat = "HH:mm[:ss[XXX]]";
      this.dateTimeFormat = "yyyy-MM-dd'T'HH:mm[:ss[XXX]]";
      this.documentEncoding = Charset.defaultCharset();
   }

   public static SystemEnvironment ofSystemDefaults() {
      return new SystemEnvironment();
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends SystemEnvironment> defineDomainFields() {
      return (Fields<SystemEnvironment>) Environment.super.defineDomainFields();
   }

   @Getter
   public ZoneId timeZone() {
      return timeZone;
   }

   @Getter
   public Locale locale() {
      return locale;
   }

   @Getter
   public String dateFormat() {
      return dateFormat;
   }

   @Getter
   public String timeFormat() {
      return timeFormat;
   }

   @Getter
   public String dateTimeFormat() {
      return dateTimeFormat;
   }

   @Getter
   public Charset documentEncoding() {
      return documentEncoding;
   }

}
