package com.tinubu.document.generator.asciidoctor.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_ASCIIDOC;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.core.template.TextTemplate;

public class AsciidocTemplate extends TextTemplate {

   protected AsciidocTemplate(AsciidocTemplateBuilder builder) {
      super(builder);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends AsciidocTemplate> defineDomainFields() {
      return Fields
            .<AsciidocTemplate>builder()
            .superFields((Fields<AsciidocTemplate>) super.defineDomainFields())
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return isTypeAndSubtypeEqualTo(value(TEXT_ASCIIDOC));
   }

   public static AsciidocTemplateBuilder reconstituteBuilder() {
      return new AsciidocTemplateBuilder().reconstitute();
   }

   public static class AsciidocTemplateBuilder extends TextTemplateBuilder {

      public static AsciidocTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new AsciidocTemplateBuilder()
               .templateId(template.templateId())
               .templateRepository(template.templateRepository());
      }

      public static AsciidocTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return AsciidocTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (AsciidocTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder templateId(DocumentPath templateId) {
         return (AsciidocTemplateBuilder) super.templateId(templateId);
      }

      @Override
      public AsciidocTemplateBuilder templateContent(String templateContent) {
         return (AsciidocTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder headerId(DocumentPath headerId) {
         return (AsciidocTemplateBuilder) super.headerId(headerId);
      }

      @Override
      public AsciidocTemplateBuilder headerContent(String header) {
         return (AsciidocTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder footerId(DocumentPath footerId) {
         return (AsciidocTemplateBuilder) super.footerId(footerId);
      }

      @Override
      public AsciidocTemplateBuilder footerContent(String footer) {
         return (AsciidocTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public AsciidocTemplate buildDomainObject() {
         return new AsciidocTemplate(this);
      }

      @Override
      public AsciidocTemplate build() {
         return (AsciidocTemplate) super.build();
      }

   }
}
