package com.tinubu.document.generator.asciidoctor.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_YAML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.instance;

import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;
import com.tinubu.document.generator.core.skin.Skin;

public class PdfThemeSkin extends AbstractValue implements Skin {
   protected final DocumentPath skinId;
   protected final DocumentRepository skinRepository;
   protected final DocumentPath logoId;
   protected final DocumentPath watermarkId;

   protected PdfThemeSkin(PdfThemeSkinBuilder builder) {
      this.skinId = builder.skinId;
      this.skinRepository = builder.skinRepository;
      this.logoId = builder.logoId;
      this.watermarkId = builder.watermarkId;
   }

   @SuppressWarnings("unchecked")
   public Fields<? extends PdfThemeSkin> defineDomainFields() {
      return Fields
            .<PdfThemeSkin>builder()
            .superFields((Fields<PdfThemeSkin>) super.defineDomainFields())
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedSkinContentType() {
      return isTypeAndSubtypeEqualTo(value(APPLICATION_YAML));
   }

   @Getter
   @Override
   public Optional<DocumentPath> skinId() {
      return nullable(skinId);
   }

   @Getter
   @Override
   public DocumentRepository skinRepository() {
      return skinRepository;
   }

   @Getter
   public Optional<DocumentPath> logoId() {
      return nullable(logoId);
   }

   @Getter
   public Optional<DocumentPath> watermarkId() {
      return nullable(watermarkId);
   }

   /**
    * Provides automatic configuration for this theme with sensible defaults.
    *
    * @return attributes maps
    *
    * @implSpec The values should not be prefixed with {@code @} here as
    *       the decision is deferred to calling code.
    * @implSpec Automatic attributes should not override theme configuration, so, fo example, logo and
    *       watermark are only applied if no theme is present.
    */
   public Map<String, Object> automaticAttributes() {
      Map<String, Object> attributes = new LinkedHashMap<>();

      skinId().ifPresentOrElse(skinId -> {
         attributes.put("pdf-theme", skinId.stringValue());
         attributes.put("pdf-themesdir", "{docdir}");
      }, () -> {
         logoId().ifPresent(logoId -> {
            attributes.put("title-logo-image",
                           "image:"
                           + logoId.stringValue()
                           + "[top=0px,float=right,align=center,pdfwidth=30%]");
         });

         watermarkId().ifPresent(watermarkId -> {
            for (String watermarkAttribute : list("page-foreground-image"))
               attributes.put(watermarkAttribute,
                              "image:" + watermarkId.stringValue() + "[fit=none,pdfwidth=70%]");
         });
      });

      return attributes;
   }

   /**
    * No-op {@link PdfThemeSkin} instance.
    *
    * @return no-op skin
    */
   public static PdfThemeSkin noopSkin() {
      class NoopSkin extends PdfThemeSkin {
         protected NoopSkin() {
            super(new PdfThemeSkinBuilder().skinRepository(new NoopDocumentRepository()).finalizeBuild());
         }
      }

      return new NoopSkin();
   }

   public static PdfThemeSkinBuilder reconstituteBuilder() {
      return new PdfThemeSkinBuilder().reconstitute();
   }

   public static class PdfThemeSkinBuilder extends DomainBuilder<PdfThemeSkin> {
      protected DocumentRepository rawContentLayer;
      protected DocumentRepository skinRepository;
      protected DocumentPath skinId;
      protected DocumentPath logoId;
      protected DocumentPath watermarkId;

      @Setter
      public PdfThemeSkinBuilder skinRepository(DocumentRepository skinRepository) {
         this.skinRepository = skinRepository;
         return this;
      }

      @Setter
      public PdfThemeSkinBuilder skinId(DocumentPath skinId) {
         this.skinId = skinId;
         return this;
      }

      public PdfThemeSkinBuilder skinContent(String skinContent) {
         this.rawContentLayer =
               nullable(rawContentLayer, () -> instance().createDocumentRepository("pdf-theme-skin-content"));
         Document rawContentDocument = rawContentDocument("pdf-theme-skin", skinContent);
         this.rawContentLayer.saveDocument(rawContentDocument, false).orElseThrow();
         this.skinId = rawContentDocument.documentId();
         return this;
      }

      @Setter
      public PdfThemeSkinBuilder logoId(DocumentPath logoId) {
         this.logoId = logoId;
         return this;
      }

      @Setter
      public PdfThemeSkinBuilder watermarkId(DocumentPath watermarkId) {
         this.watermarkId = watermarkId;
         return this;
      }

      @Override
      @SuppressWarnings("unchecked")
      public PdfThemeSkinBuilder finalizeBuild() {
         if (rawContentLayer != null) {
            this.skinRepository =
                  nullable(skinRepository, r -> r.stackRepository(rawContentLayer, true), rawContentLayer);
         }
         return this;
      }

      @Override
      public PdfThemeSkin buildDomainObject() {
         return new PdfThemeSkin(this);
      }

      private Document rawContentDocument(String name, String rawContent) {
         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(Uuid
                                                                      .newNameBasedSha1Uuid(name)
                                                                      .stringValue()))
                                    .streamContent(rawContent, StandardCharsets.UTF_8)
                                    .contentType(APPLICATION_YAML)
                                    .build());
      }

   }
}
