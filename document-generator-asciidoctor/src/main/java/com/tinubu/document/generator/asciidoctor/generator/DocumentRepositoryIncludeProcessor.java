package com.tinubu.document.generator.asciidoctor.generator;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.IncludeProcessor;
import org.asciidoctor.extension.PreprocessorReader;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Asciidoctor processor to reference documents from a repository in "include" directives.
 */
public class DocumentRepositoryIncludeProcessor extends IncludeProcessor {

   private final DocumentRepository includeDocumentRepository;
   private final Charset defaultEncoding;

   public DocumentRepositoryIncludeProcessor(Map<String, Object> config,
                                             DocumentRepository includeDocumentRepository,
                                             Charset defaultEncoding) {
      super(config);
      this.includeDocumentRepository = includeDocumentRepository;
      this.defaultEncoding = defaultEncoding;
   }

   public DocumentRepositoryIncludeProcessor(DocumentRepository includeDocumentRepository,
                                             Charset defaultEncoding) {
      this(new HashMap<>(), includeDocumentRepository, defaultEncoding);
   }

   @Override
   public boolean handles(String target) {
      return target != null && !target.startsWith("uri:");
   }

   @Override
   public void process(Document document,
                       PreprocessorReader reader,
                       String target,
                       Map<String, Object> attributes) {
      com.tinubu.commons.ports.document.domain.Document include =
            includeDocumentRepository.findDocumentById(DocumentPath.of(target))
            .orElseThrow(() -> new IllegalArgumentException(String.format("Include '%s' not found", target)));

      reader.pushInclude(include.content().stringContent(defaultEncoding), target, target, 1, attributes);
   }
}
