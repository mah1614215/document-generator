package com.tinubu.document.generator.docx4j.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOCX;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory;
import com.tinubu.document.generator.core.skin.CssSkin;
import com.tinubu.document.generator.core.skin.Skin;

public class DocxSkin extends AbstractValue implements Skin {
   protected final DocumentPath skinId;
   protected final DocumentRepository skinRepository;
   protected final CssSkin htmlSkin;

   protected DocxSkin(DocxSkinBuilder builder) {
      this.skinId = builder.skinId;
      this.skinRepository = builder.skinRepository;
      this.htmlSkin = builder.htmlSkin;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocxSkin> defineDomainFields() {
      return Fields
            .<DocxSkin>builder()
            .superFields((Fields<DocxSkin>) super.defineDomainFields())
            .field("htmlSkin", v -> v.htmlSkin)
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedSkinContentType() {
      return isTypeAndSubtypeEqualTo(value(APPLICATION_OOXML_DOCX));
   }

   @Getter
   @Override
   public Optional<DocumentPath> skinId() {
      return nullable(skinId);
   }

   @Getter
   @Override
   public DocumentRepository skinRepository() {
      return skinRepository;
   }

   @Getter
   @Override
   public Optional<DocumentPath> logoId() {
      return optional();
   }

   @Getter
   @Override
   public Optional<DocumentPath> watermarkId() {
      return optional();
   }

   /**
    * Provides an optional, custom, CSS skin to generator when output is set to HTML.
    *
    * @return HTML output skin
    */
   @Getter
   public Optional<CssSkin> htmlOutputSkin() {
      return nullable(htmlSkin);
   }

   /**
    * No-op {@link DocxSkin} instance.
    *
    * @return no-op skin
    */
   public static DocxSkin noopSkin() {
      class NoopSkin extends DocxSkin {
         protected NoopSkin() {
            super(new DocxSkinBuilder().skinRepository(new NoopDocumentRepository()).finalizeBuild());
         }
      }

      return new NoopSkin();
   }

   public static DocxSkinBuilder reconstituteBuilder() {
      return new DocxSkinBuilder().reconstitute();
   }

   public static class DocxSkinBuilder extends DomainBuilder<DocxSkin> {
      private DocumentRepository rawContentLayer;
      private DocumentRepository skinRepository;
      private DocumentPath skinId;
      private CssSkin htmlSkin;

      @Setter
      public DocxSkinBuilder skinId(DocumentPath skinId) {
         this.skinId = skinId;
         return this;
      }

      public DocxSkinBuilder skinContent(String skinContent) {
         this.rawContentLayer = nullable(rawContentLayer,
                                         () -> GeneratedDocumentRepositoryFactory
                                               .instance()
                                               .createDocumentRepository("docx-skin-content"));
         Document rawContentDocument = rawContentDocument("skin", skinContent);
         this.rawContentLayer.saveDocument(rawContentDocument, false).orElseThrow();
         this.skinId = rawContentDocument.documentId();
         return this;
      }

      @Setter
      public DocxSkinBuilder skinRepository(DocumentRepository skinRepository) {
         this.skinRepository = skinRepository;
         return this;
      }

      @Setter
      public DocxSkinBuilder htmlSkin(CssSkin htmlSkin) {
         this.htmlSkin = htmlSkin;
         return this;
      }

      @Override
      @SuppressWarnings("unchecked")
      public DocxSkinBuilder finalizeBuild() {
         if (rawContentLayer != null) {
            this.skinRepository =
                  nullable(skinRepository, r -> r.stackRepository(rawContentLayer, true), rawContentLayer);
         }
         return this;
      }

      @Override
      public DocxSkin buildDomainObject() {
         return new DocxSkin(this);
      }

      private Document rawContentDocument(String name, String rawContent) {
         return new DocumentBuilder()
               .documentId(DocumentPath.of(Uuid.newNameBasedSha1Uuid(name).stringValue()))
               .streamContent(rawContent, StandardCharsets.UTF_8).contentType(APPLICATION_OOXML_DOCX)
               .build();
      }

   }
}
