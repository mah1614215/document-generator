package com.tinubu.document.generator.docx4j.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOCX;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOTX;
import static com.tinubu.commons.lang.util.CollectionUtils.list;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.core.template.TextTemplate;

public class DocxTemplate extends TextTemplate {

   protected DocxTemplate(DocxTemplateBuilder builder) {
      super(builder);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocxTemplate> defineDomainFields() {
      return Fields.<DocxTemplate>builder()
            .superFields((Fields<DocxTemplate>) super.defineDomainFields())
            .field("templateId", v -> v.templateId, isNotNull())
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return withStrippedParameters(isIn(value(list(APPLICATION_OOXML_DOCX, APPLICATION_OOXML_DOTX))));
   }

   public static DocxTemplateBuilder reconstituteBuilder() {
      return new DocxTemplateBuilder().reconstitute();
   }

   public static class DocxTemplateBuilder extends TextTemplateBuilder {

      public static DocxTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new DocxTemplateBuilder()
               .templateId(template.templateId())
               .templateRepository(template.templateRepository());
      }

      public static DocxTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull());

         return ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      @Setter
      public DocxTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (DocxTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public DocxTemplateBuilder templateId(DocumentPath templateId) {
         return (DocxTemplateBuilder) super.templateId(templateId);
      }

      @Override
      public DocxTemplateBuilder templateContent(String templateContent) {
         return (DocxTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public DocxTemplateBuilder headerId(DocumentPath headerId) {
         return (DocxTemplateBuilder) super.headerId(headerId);
      }

      @Override
      public DocxTemplateBuilder headerContent(String header) {
         return (DocxTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public DocxTemplateBuilder footerId(DocumentPath footerId) {
         return (DocxTemplateBuilder) super.footerId(footerId);
      }

      @Override
      public DocxTemplateBuilder footerContent(String footer) {
         return (DocxTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public DocxTemplate buildDomainObject() {
         return new DocxTemplate(this);
      }

      @Override
      public DocxTemplate build() {
         return (DocxTemplate) super.build();
      }

   }

}
