package com.tinubu.document.generator.docx4j.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.docx4j.skin.DocxSkin;

public class MergePackageProcessor implements PackageProcessor {

   private final List<Object> mergeContent;

   public MergePackageProcessor(List<Object> mergeContent) {
      this.mergeContent = notNull(mergeContent, "mergeContent");
   }

   public MergePackageProcessor(WordprocessingMLPackage mergePackage) {
      this(notNull(mergePackage, "mergePackage").getMainDocumentPart().getContent());
   }

   @Override
   public WordprocessingMLPackage process(WordprocessingMLPackage wordPackage,
                                          Template template,
                                          TextModel model,
                                          DocxSkin skin) {
      notNull(wordPackage, "wordPackage");
      notNull(template, "template");
      notNull(model, "model");
      notNull(skin, "skin");

      wordPackage.getMainDocumentPart().getContent().addAll(mergeContent);
      return wordPackage;
   }
}
