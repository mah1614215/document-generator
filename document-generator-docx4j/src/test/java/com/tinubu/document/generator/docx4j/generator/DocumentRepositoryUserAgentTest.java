package com.tinubu.document.generator.docx4j.generator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;

class DocumentRepositoryUserAgentTest {

   DocumentRepositoryUserAgent userAgent;

   @BeforeEach
   void init() {
      userAgent = new DocumentRepositoryUserAgent(new MemoryDocumentRepository());
   }

   @Test
   void setBaseURL() {
      userAgent.setBaseURL(".");
      assertThat(userAgent.resolveURI("file.txt")).isEqualTo("file.txt");
   }

   @Test
   void testUserAgentWhenNullBaseUrl() {
      userAgent.setBaseURL(null);
      assertThat(userAgent.resolveURI("file.txt")).isEqualTo("file.txt");
   }

   @Test
   void testUserAgentWhenAbsoluteBaseUrl() {
      userAgent.setBaseURL("/path");
      assertThat(userAgent.resolveURI("file.txt")).isEqualTo("path/file.txt");
   }

   @Test
   void setBaseURLFromURI() {
      userAgent.setBaseURL("path/sub/");
      assertThat(userAgent.resolveURI("file.txt")).isEqualTo("path/sub/file.txt");
   }

   @Test
   void setBaseURLFromURIWithScheme() {
      userAgent.setBaseURL("file:/path/");
      assertThat(userAgent.resolveURI("file.txt")).isEqualTo("file:/path/file.txt");
   }

   @Test
   void testNullUri() {
      assertThat(userAgent.resolveURI(null)).isNull();
   }

   @Test
   void testMalFormedUri() {
      userAgent.setBaseURL(".");
      assertThat(userAgent.resolveURI("\\uri")).isNull();
   }

   @Test
   void testMalFormedBaseUrl() {
      userAgent.setBaseURL("\\.");
      assertThatExceptionOfType(NullPointerException.class)
            .isThrownBy(() -> userAgent.resolveURI("file"))
            .withMessage("Cannot invoke \"String.length()\" because \"this.input\" is null");
   }

   @Test
   void openStreamWithFakeURI() {
      assertThat(userAgent.openStream("file:/file.txt")).isNull();
   }

   @Test
   void openStreamMalformedURI() {
      assertThat(userAgent.openStream("file:/kjbjkbkj|abc"))
            .isNull();
   }

   @Test
   void openStreamWithACorrectFile() {
      assertThat(userAgent.openStream("template.html")).isNull();
   }

   @Test
   void openReaderWithFakeURI() {
      assertThat(userAgent.openReader("file:/file.txt")).isNull();
   }

   @Test
   void openReaderWithACorrectFile() {
      assertThat(userAgent.openReader("template.html")).isNull();
   }

   @Test
   void openReaderMalformedURI() {
      assertThat(userAgent.openReader("file:/efg|abc"))
            .isNull();
   }

}